# Autokey

Simple, client side scripting for Minecraft.

## Installation

⚠️ This mod requires the [Fabric API](https://modrinth.com/mod/fabric-api).  
You can install Autokey from [Modrinth](https://modrinth.com/mod/autokey) (recommended)  
or [Curseforge](https://www.curseforge.com/minecraft/mc-mods/autokey)

## Wiki

Visit the wiki [here](https://gitlab.com/tobilinz/autokey/-/wikis/home).

## Reporting Issues and Suggesting features

Please make sure that no one else has already reported your issue / suggested your idea first.  
You can report issues [here](https://gitlab.com/tobilinz/autokey/-/issues).  

## Contact
E-Mail: contact@tobl.in
