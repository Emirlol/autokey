package com.cubicequation.autokey_core.language.functions.runnables;

import com.cubicequation.autokey_core.language.exceptions.RuntimeException;
import org.jetbrains.annotations.NotNull;

public interface NoReturnRunnable
{
    void run(@NotNull Object @NotNull [] arguments) throws RuntimeException;
}
