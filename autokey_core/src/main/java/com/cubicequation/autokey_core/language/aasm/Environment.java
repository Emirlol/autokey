package com.cubicequation.autokey_core.language.aasm;

import org.jetbrains.annotations.NotNull;

import java.util.*;

public final class Environment
{
    private final HashMap<String, String> variables;

    public Environment(final @NotNull HashMap<String, String> variables)
    {
        this.variables = variables;
    }

    public String getVariable(final @NotNull String name)
    {
        return variables.get(name);
    }

    public Map<String, String> getAllVariables()
    {
        return Collections.unmodifiableMap(variables);
    }
}
