package com.cubicequation.autokey_core.language.aasm;

import com.cubicequation.autokey_core.language.exceptions.AutokeyException;
import org.jetbrains.annotations.NotNull;

public class AasmException extends AutokeyException
{
    public AasmException(final @NotNull String message, final int line, final int position)
    {
        super(message);
        this.line = line;
        this.position = position;
    }

    public AasmException(final @NotNull String message, final int line, final int position, final @NotNull Object @NotNull ... data)
    {
        super(message, data);
        this.line = line;
        this.position = position;
    }
}
