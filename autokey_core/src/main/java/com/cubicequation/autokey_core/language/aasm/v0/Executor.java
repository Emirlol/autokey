package com.cubicequation.autokey_core.language.aasm.v0;

import com.cubicequation.autokey_core.language.Data;
import com.cubicequation.autokey_core.language.Library;
import com.cubicequation.autokey_core.language.aasm.AasmException;
import com.cubicequation.autokey_core.language.aasm.Environment;
import com.cubicequation.autokey_core.language.exceptions.RuntimeException;
import com.cubicequation.autokey_core.language.functions.Function;
import com.cubicequation.autokey_core.language.functions.Overload;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;

public final class Executor
{
    private Executor()
    {
        throw new IllegalStateException("Executor cannot be instantiated");
    }

    @Contract("_ -> new")
    private static @NotNull AasmException unexpectedTokenException(final @NotNull Token token)
    {
        return new AasmException("autokey_core.language.exception.unexpected_token", token.line(), token.start(), token.value(), token.type());
    }

    @Contract("_ -> new")
    private static @NotNull AasmException missingTokenException(final @NotNull Token token)
    {
        return new AasmException("autokey_core.language.exception.missing_token", token.line(), token.start() + token.length(), token.value(), token.type());
    }

    public static void execute(final Token @NotNull [][] lines, final @NotNull HashMap<String, Integer> labels, final @NotNull Environment environment, final @NotNull Interrupter interrupter) throws AasmException, RuntimeException
    {
        final VariableMapStack stack = new VariableMapStack();

        final VariableMap envVars = new VariableMap();
        envVars.pushAll(environment.getAllVariables());

        //Backwards Compatibility
        final String message = environment.getVariable("chatMessage");
        if (message != null) envVars.push("message", message);

        stack.addElement(envVars);

        for (int i = 0; i < lines.length && !interrupter.interrupted; i++)
        {
            final Token[] line = lines[i];

            if (line.length == 0) continue;

            switch (line[0].type())
            {
                case IDENTIFIER ->
                {
                    final Token first = line[0];

                    final Function function = Library.getFunction(first.value());
                    if (function == null)
                        throw new AasmException("autokey_core.language.aasm.function_not_found", first.line(), first.start(), first.value());

                    final ArrayList<Object> inputs = new ArrayList<>();
                    final ArrayList<Class<?>> inputTypes = new ArrayList<>();
                    final ArrayList<String> outputs = new ArrayList<>();

                    for (int j = 1; j < line.length; j++)
                    {
                        if (line[j].type() == TokenType.OUTPUT && j + 1 < line.length)
                        {
                            j++;
                            outputs.add(line[j].value());
                        }
                        else
                        {
                            final Object data = stack.get(line[j]);
                            inputs.add(data);
                            inputTypes.add(data.getClass());
                        }

                        if (line[j].type() != TokenType.IDENTIFIER) throw unexpectedTokenException(line[j]);
                    }

                    final Optional<Overload> optionalOverload = function.getOverload(inputTypes.toArray(Class[]::new));
                    if (optionalOverload.isEmpty())
                    {
                        StringBuilder sb = new StringBuilder();
                        for (Class<?> clazz : inputTypes)
                        {
                            sb.append(clazz.getSimpleName());
                            sb.append(", ");
                        }

                        sb.delete(sb.length() - 2, sb.length());

                        throw new AasmException("autokey_core.language.aasm.overload_not_found", first.line(), first.start(), first.value(), sb.toString());
                    }

                    final Overload overload = optionalOverload.get();

                    if (overload.getReturnType() == Void.class && !outputs.isEmpty())
                        throw new AasmException("autokey_core.language.aasm.no_return_value", first.line(), first.start());

                    final Class<?>[] argumentTypes = overload.getArgumentTypes();
                    for (int k = 0; k < inputs.size(); k++)
                        inputs.set(k, Cast.cast(argumentTypes[k], inputs.get(k)));

                    final Optional<Object> result = overload.run(inputs.toArray(Object[]::new));

                    if (result.isEmpty()) break;

                    for (final String location : outputs) stack.push(location, result.get());
                }
                case DATA ->
                {
                    if (line.length < 2) throw missingTokenException(line[0]);

                    final Object value = Data.fromString(line[0].value());

                    for (int j = 1; j < line.length; j++)
                    {
                        if (line[j].type() != TokenType.IDENTIFIER) throw unexpectedTokenException(line[j]);
                        stack.push(line[j].value(), value);
                    }
                }
                case FREE ->
                {
                    if (line.length < 2) throw missingTokenException(line[0]);

                    for (int j = 1; j < line.length; j++)
                    {
                        final String key = stack.get(String.class, line[j]);
                        if (line[j].type() != TokenType.IDENTIFIER) throw unexpectedTokenException(line[j]);
                        stack.pop(key, line[j]);
                    }
                }
                case JUMPIF ->
                {
                    if (line.length < 3) throw missingTokenException(line[1]);
                    else if (line.length > 3) throw unexpectedTokenException(line[3]);

                    final boolean condition = stack.get(Boolean.class, line[1]);
                    final String destination = stack.get(String.class, line[2]);

                    if (!condition) break;

                    if (!labels.containsKey(destination))
                        throw new AasmException("autokey_core.language.aasm.label_does_not_exist", line[1].line(), line[1].start(), line[1].value());
                    i = labels.get(destination) - 1;
                }
                case JUMP ->
                {
                    if (line.length < 2) throw missingTokenException(line[0]);
                    else if (line.length > 2) throw unexpectedTokenException(line[2]);

                    final String destination = stack.get(String.class, line[1]);

                    if (!labels.containsKey(destination))
                        throw new AasmException("autokey_core.language.aasm.label_does_not_exist", line[1].line(), line[1].start(), line[1].value());
                    i = labels.get(destination) - 1;
                }
                case SET ->
                {
                    if (line.length < 3) throw missingTokenException(line[1]);
                    if (line[1].type() != TokenType.IDENTIFIER) throw unexpectedTokenException(line[1]);

                    final Object data = stack.get(line[1]);

                    for (int j = 2; j < line.length; j++)
                    {
                        if (line[j].type() != TokenType.IDENTIFIER) throw unexpectedTokenException(line[j]);
                        stack.push(line[j].value(), data);
                    }
                }
                case NEW_STACK -> stack.addElement(new VariableMap());
                case STACK_AMOUNT ->
                {
                    if (line.length < 2) throw missingTokenException(line[0]);

                    for (int j = 1; j < line.length; j++)
                    {
                        if (line[j].type() != TokenType.IDENTIFIER) throw unexpectedTokenException(line[j]);
                        stack.push(line[j].value(), stack.size());
                    }
                }
                case STACK ->
                {
                    if (line.length < 2) throw missingTokenException(line[0]);
                    else if (line.length > 2) throw unexpectedTokenException(line[2]);

                    final int level = stack.get(Integer.class, line[1]);

                    if (level < 1)
                        throw new AasmException("autokey_core.language.aasm.no_previous_stack", line[1].line(), line[1].start());

                    while (stack.size() > level) stack.pop();
                }
                case LABEL ->
                {
                }
                case VAR ->
                {
                    if (line.length < 3) throw missingTokenException(line[0]);
                    if (line[1].type() != TokenType.DATA) throw unexpectedTokenException(line[1]);

                    final Object value = Data.fromString(line[1].value());

                    for (int j = 2; j < line.length; j++)
                    {
                        if (line[j].type() != TokenType.IDENTIFIER) throw unexpectedTokenException(line[j]);
                        stack.pushToTop(line[j].value(), value);
                    }
                }
                default -> throw unexpectedTokenException(line[0]);
            }
        }
    }

    public static class Interrupter
    {
        private volatile boolean interrupted = false;

        public synchronized void interrupt()
        {
            interrupted = true;
        }

    }
}
