package com.cubicequation.autokey_core.language.functions;

import com.cubicequation.autokey_core.language.functions.runnables.NoReturnRunnable;
import com.cubicequation.autokey_core.language.functions.runnables.ReturnRunnable;
import com.cubicequation.autokey_core.language.functions.runnables.Runnable;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class LibraryBuilder
{
    protected final HashMap<String, ArrayList<Overload>> functions = new HashMap<>();

    private void addOverload(final @NotNull String name, final @NotNull Runnable runnable, final Class<?> returnType, final @NotNull Class<?> @NotNull ... argumentTypes)
    {
        final ArrayList<Overload> function = functions.computeIfAbsent(name, k -> new ArrayList<>());
        function.add(new Overload(runnable, returnType, argumentTypes));
    }

    public <T> void addROverload(final @NotNull String name, final @NotNull ReturnRunnable<T> runnable, final Class<T> returnType, final @NotNull Class<?> @NotNull ... argumentTypes)
    {
        addOverload(name, arguments -> Optional.ofNullable(runnable.run(arguments)), returnType, argumentTypes);
    }

    public void addNROverload(final @NotNull String name, final @NotNull NoReturnRunnable runnable, final @NotNull Class<?> @NotNull ... argumentTypes)
    {
        addOverload(name, arguments ->
        {
            runnable.run(arguments);
            return Optional.empty();
        }, Void.class, argumentTypes);
    }

    public Map<String, Function> build()
    {
        final HashMap<String, Function> functions = new HashMap<>();
        this.functions.forEach((key, value) -> functions.put(key, new Function(value.toArray(Overload[]::new))));
        return functions;
    }
}
