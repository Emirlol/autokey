package com.cubicequation.autokey_core.language.aasm.v0;

import java.util.HashMap;
import java.util.Map;

final class VariableMap
{
    private final HashMap<String, Object> map;

    VariableMap()
    {
        map = new HashMap<>();
    }

    void push(final String key, final Object value)
    {
        map.put(key, value);
    }

    void pushAll(final Map<String, String> map)
    {
        this.map.putAll(map);
    }

    boolean contains(final String key)
    {
        return map.containsKey(key);
    }

    Object get(final String key)
    {
        return map.get(key);
    }

    void pop(final String key)
    {
        map.remove(key);
    }
}
