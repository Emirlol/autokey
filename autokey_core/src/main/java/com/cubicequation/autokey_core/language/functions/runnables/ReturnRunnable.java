package com.cubicequation.autokey_core.language.functions.runnables;

import com.cubicequation.autokey_core.language.exceptions.RuntimeException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ReturnRunnable<T>
{
    @Nullable T run(@NotNull Object @NotNull [] arguments) throws RuntimeException;
}
