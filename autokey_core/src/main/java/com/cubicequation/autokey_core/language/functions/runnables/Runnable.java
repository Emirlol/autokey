package com.cubicequation.autokey_core.language.functions.runnables;

import com.cubicequation.autokey_core.language.exceptions.RuntimeException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public interface Runnable
{
    @NotNull Optional<@Nullable Object> run(@NotNull Object @NotNull [] arguments) throws RuntimeException;
}
