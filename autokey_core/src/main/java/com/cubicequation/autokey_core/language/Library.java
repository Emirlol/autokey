package com.cubicequation.autokey_core.language;

import com.cubicequation.autokey_core.api.LibraryAPI;
import com.cubicequation.autokey_core.entrypoints.Autokey;
import com.cubicequation.autokey_core.api.AutokeyAPI;
import com.cubicequation.autokey_core.language.functions.Function;
import net.fabricmc.loader.api.metadata.ModMetadata;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;

public final class Library
{
    private static final HashMap<String, Function> FUNCTIONS = new HashMap<>();

    private Library()
    {
        throw new IllegalStateException("Library cannot be instantiated");
    }

    public static void updateFunctions(Autokey.@NotNull MessageConsumer consumer)
    {
        consumer.consume("Loading libraries...", Autokey.MessageConsumer.Severity.INFO);

        FUNCTIONS.clear();
        AutokeyAPI.iterateAPIs("autokey_core", LibraryAPI.class, (LibraryAPI api, ModMetadata metadata) ->
        {
            final Map<String, ? extends Function> functions = api.getFunctions();
            FUNCTIONS.putAll(functions);
            consumer.consume(String.format("Loaded %d functions from %s.", functions.size(), metadata.getName()), Autokey.MessageConsumer.Severity.INFO);
        });

        consumer.consume("Successfully loaded libraries.", Autokey.MessageConsumer.Severity.INFO);
    }

    public static @Nullable Function getFunction(final String name)
    {
        return FUNCTIONS.get(name);
    }

    public static boolean containsFunction(final String name)
    {
        return FUNCTIONS.containsKey(name);
    }
}