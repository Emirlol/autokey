package com.cubicequation.autokey_core.language.aasm.v0;

import com.cubicequation.autokey_core.language.Data;
import com.cubicequation.autokey_core.util.StringUtil;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

final class Lexer
{
    private Lexer()
    {
        throw new IllegalStateException("Lexer cannot be instantiated");
    }

    @Contract("_ -> new")
    static @NotNull LexerData getTokens(final @NotNull Scanner scanner)
    {
        final ArrayList<ArrayList<Token>> tokens = new ArrayList<>();
        final HashMap<String, Integer> labels = new HashMap<>();

        int lineCount = 0;
        ArrayList<Token> currentLine;
        StringBuilder lexeme = new StringBuilder();
        boolean isString = false;

        while (scanner.hasNextLine())
        {
            lineCount++;
            final StringBuilder line = new StringBuilder(scanner.nextLine());

            if (isString) lexeme.append('\n');

            currentLine = new ArrayList<>();
            tokens.add(currentLine);

            if (line.isEmpty()) continue;

            final Integer[] spacePositions = StringUtil.getSpacePositions(line);

            for (int position = 0; position < line.length(); position++)
            {
                char character = line.charAt(position);

                if (Character.isWhitespace(character) && !isString)
                {
                    if (lexeme.isEmpty()) continue;

                    final String subLine = line.substring(0, position)
                            .replace(" ", "");
                    final StringBuilder subOriginalBuilder = new StringBuilder(subLine);

                    for (int pos : spacePositions)
                    {
                        if (pos >= subOriginalBuilder.length()) break;
                        subOriginalBuilder.insert(pos, ' ');
                    }

                    Token token = getToken(lexeme.toString(), lineCount, subOriginalBuilder.length());
                    if (currentLine.isEmpty() && token.type() == TokenType.LABEL)
                        labels.put(token.value(), lineCount);
                    currentLine.add(token);

                    lexeme = new StringBuilder();
                    continue;
                }
                else if (character == '>' && !isString) line.insert(position + 1, ' ');
                else if (character == '#' && !isString) break;
                else if (character == '"')
                {
                    isString = !isString;

                    if (isString && !lexeme.isEmpty())
                    {
                        final String subLine = line.substring(0, position)
                                .replace(" ", "");
                        final StringBuilder subOriginalBuilder = new StringBuilder(subLine);

                        for (int pos : spacePositions)
                        {
                            if (pos >= subOriginalBuilder.length()) break;
                            subOriginalBuilder.insert(pos, ' ');
                        }

                        Token token = getToken(lexeme.toString(), lineCount, subOriginalBuilder.length());
                        if (currentLine.isEmpty() && token.type() == TokenType.LABEL)
                            labels.put(token.value(), lineCount);
                        currentLine.add(token);

                        lexeme = new StringBuilder();
                    }
                    else if (!isString) line.insert(position + 1, ' ');
                }

                lexeme.append(character);
            }

            if (!lexeme.isEmpty() && !isString)
            {
                Token token = getToken(lexeme.toString(), lineCount, line.length());
                if (currentLine.isEmpty() && token.type() == TokenType.LABEL) labels.put(token.value(), lineCount);
                currentLine.add(token);
                lexeme = new StringBuilder();
            }
        }

        final Token[][] lines = tokens.stream()
                .map(u -> u.toArray(Token[]::new))
                .toArray(Token[][]::new);
        return new LexerData(lines, labels);
    }

    @Contract("_, _, _ -> new")
    private static @NotNull Token getToken(@NotNull String lexeme, final int line, final int position)
    {
        final int pos = position - lexeme.length();

        TokenType tokenType = switch (lexeme)
        {
            case ">" -> TokenType.OUTPUT;
            case "jmpif" -> TokenType.JUMPIF;
            case "jmp" -> TokenType.JUMP;
            case "free" -> TokenType.FREE;
            case "set" -> TokenType.SET;
            case "newStack" -> TokenType.NEW_STACK;
            case "stackAmount" -> TokenType.STACK_AMOUNT;
            case "stack" -> TokenType.STACK;
            case "var" -> TokenType.VAR;
            default -> TokenType.IDENTIFIER;
        };
        if (tokenType != TokenType.IDENTIFIER) return new Token(tokenType, lexeme, line, pos, lexeme.length());


        if (lexeme.endsWith(":"))
        {
            tokenType = TokenType.LABEL;
            lexeme = lexeme.substring(0, lexeme.length() - 1);
        }
        else if (Data.isData(lexeme)) tokenType = TokenType.DATA;
        return new Token(tokenType, lexeme, line, pos, lexeme.length());
    }

    record LexerData(@NotNull Token @NotNull [] @NotNull [] lines, @NotNull HashMap<String, Integer> labels)
    {
    }
}
