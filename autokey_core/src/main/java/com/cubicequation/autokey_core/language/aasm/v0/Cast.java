package com.cubicequation.autokey_core.language.aasm.v0;

import org.jetbrains.annotations.NotNull;

final class Cast
{
    @SuppressWarnings("unchecked")
    static <T> T cast(@NotNull Class<T> tClass, Object object)
    {
        if (tClass.isInstance(object)) return (T)object;

        if (object instanceof Number number && Number.class.isAssignableFrom(tClass))
        {
            if (tClass == Integer.class) return (T)(Integer)number.intValue();
            if (tClass == Long.class) return (T)(Long)number.longValue();
            if (tClass == Float.class) return (T)(Float)number.floatValue();
            if (tClass == Double.class) return (T)(Double)number.doubleValue();
            if (tClass == Byte.class) return (T)(Byte)number.byteValue();
            if (tClass == Short.class) return (T)(Short)number.shortValue();
        }

        //TODO add message
        throw new ClassCastException();
    }
}
