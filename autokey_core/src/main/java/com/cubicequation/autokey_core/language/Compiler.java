package com.cubicequation.autokey_core.language;

import com.cubicequation.autokey_core.language.exceptions.CompilerException;
import org.jetbrains.annotations.NotNull;

import java.util.Scanner;

public interface Compiler
{
    @NotNull
    String compile(@NotNull Scanner scanner) throws CompilerException;
}
