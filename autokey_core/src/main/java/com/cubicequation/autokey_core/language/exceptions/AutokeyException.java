package com.cubicequation.autokey_core.language.exceptions;

import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import org.jetbrains.annotations.NotNull;

public class AutokeyException extends Exception
{
    private static final MutableText MESSAGE_TEXT = Text.translatable("autokey_core.language.exception.at_pos");

    public static @NotNull String prettyPrint(final @NotNull Exception exception)
    {
        String message = exception.getClass().getSimpleName() + ": " + exception.getLocalizedMessage();

        if (exception instanceof AutokeyException autokeyException)
        {
            message += '\n';
            message += String.format(
                    MESSAGE_TEXT.getString().replace('€', '%'),
                    autokeyException.line,
                    autokeyException.position);
        }

        return message;
    }

    protected int line;
    protected int position;
    private final @NotNull Object[] data;

    public AutokeyException(final @NotNull String message)
    {
        super(message);
        this.data = new Object[0];
    }

    public AutokeyException(final @NotNull String message, final @NotNull Object @NotNull ... data)
    {
        super(message);
        this.data = data;
    }

    @Override
    public String getLocalizedMessage()
    {
        return String.format(Text.translatable(getMessage()).getString().replace("€", "%"), data);
    }
}