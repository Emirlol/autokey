package com.cubicequation.autokey_core.language.aasm.v0;

import com.cubicequation.autokey_core.language.aasm.AasmException;
import org.jetbrains.annotations.NotNull;

class VariableMapStack extends java.util.Stack<VariableMap>
{
    Object get(final @NotNull Token token) throws AasmException
    {
        for (int i = size() - 1; i >= 0; i--)
        {
            final Object data = get(i).get(token.value());

            if (data != null) return data;
        }

        throw new AasmException("autokey_core.language.aasm.variable_does_not_exist", token.line(), token.start(), token.value());
    }

    <T> T get(@NotNull Class<T> tClass, Token token) throws AasmException
    {
        final Object object = get(token);
        try
        {
            return Cast.cast(tClass, object);
        }
        catch (ClassCastException e)
        {
            throw new AasmException("autokey_core.language.aasm.cast_exception", token.line(), token.start(), object.getClass().getSimpleName(), tClass.getSimpleName());
        }
    }

    void pushToTop(final String key, final Object value)
    {
        peek().push(key, value);
    }

    void push(final String key, final Object value)
    {
        for (int i = size() - 1; i >= 0; i--)
        {
            final VariableMap stack = get(i);

            if (!stack.contains(key)) continue;

            stack.push(key, value);
            return;
        }

        peek().push(key, value);
    }

    void pop(final String key, final @NotNull Token token) throws AasmException
    {
        for (int i = size() - 1; i >= 0; i--)
        {
            final VariableMap stack = get(i);

            if (!stack.contains(key)) continue;

            stack.pop(key);
            return;
        }

        throw new AasmException("autokey_core.language.aasm.variable_does_not_exist", token.line(), token.start(), key);
    }
}
