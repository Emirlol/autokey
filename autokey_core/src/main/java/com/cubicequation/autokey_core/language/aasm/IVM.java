package com.cubicequation.autokey_core.language.aasm;

public interface IVM
{
    void run(Environment environment) throws Exception;
    void interrupt();
}
