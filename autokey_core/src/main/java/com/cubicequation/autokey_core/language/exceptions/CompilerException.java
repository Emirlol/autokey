package com.cubicequation.autokey_core.language.exceptions;

import org.jetbrains.annotations.NotNull;

public class CompilerException extends AutokeyException
{
    public CompilerException(final @NotNull String message)
    {
        super(message);
    }

    public CompilerException(final @NotNull String message, final @NotNull Object @NotNull ... data)
    {
        super(message, data);
    }
}
