package com.cubicequation.autokey_core.language;

import com.cubicequation.autokey_core.api.CompilerAPI;
import com.cubicequation.autokey_core.entrypoints.Autokey;
import com.cubicequation.autokey_core.api.AutokeyAPI;
import com.cubicequation.autokey_core.language.exceptions.AutokeyException;
import com.cubicequation.autokey_core.language.exceptions.CompilerException;
import com.cubicequation.autokey_core.util.Feedback;
import com.cubicequation.autokey_core.util.Ref;
import net.fabricmc.loader.api.metadata.ModMetadata;
import net.minecraft.text.HoverEvent;
import net.minecraft.text.MutableText;
import net.minecraft.text.Style;
import net.minecraft.text.Text;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public final class Languages
{
    private static final Map<String, Compiler> COMPILERS = new HashMap<>();
    private static final MutableText NO_COMPILER_FOUND_TEXT = Text.translatable("autokey_core.file.no_matching_compiler_found");
    private static final MutableText COMPILE_ERROR_TEXT = Text.translatable("autokey_core.compile_error");

    private Languages()
    {
        throw new IllegalStateException("Languages cannot be instantiated");
    }

    public static void updateLanguages(@NotNull Autokey.MessageConsumer consumer)
    {
        consumer.consume("Loading languages...", Autokey.MessageConsumer.Severity.INFO);

        COMPILERS.clear();

        AutokeyAPI.iterateAPIs("autokey_core", CompilerAPI.class, (CompilerAPI api, ModMetadata metadata) ->
        {
            final Ref<Integer> compilerCount = new Ref<>(0);

            api.getCompilers()
                    .forEach((compilerName, compiler) ->
                    {
                        if (Objects.equals(compilerName, "aasm") || COMPILERS.containsKey(compilerName))
                        {
                            consumer.consume(String.format("%s provides a language that has already been added. (%s)", metadata.getName(), compilerName), Autokey.MessageConsumer.Severity.ERROR);
                            return;
                        }

                        COMPILERS.put(compilerName, compiler);
                        consumer.consume("Loaded " + compilerName + ".", Autokey.MessageConsumer.Severity.INFO);
                        compilerCount.setValue(compilerCount.getValue() + 1);
                    });

            if (compilerCount.getValue() > 1)
                consumer.consume(String.format("Loaded %d languages from %s.", compilerCount.getValue(), metadata.getName()), Autokey.MessageConsumer.Severity.INFO);
        });

        consumer.consume("Successfully loaded languages.", Autokey.MessageConsumer.Severity.INFO);
    }

    public static boolean hasCompiler(String fileEnding)
    {
        return COMPILERS.containsKey(fileEnding);
    }

    public static Optional<String> compile(final @NotNull String compilerType, final @NotNull String name, final @NotNull Scanner scanner)
    {
        final Compiler compiler = COMPILERS.get(compilerType);

        if (compiler == null)
        {
            Feedback.sendClientMessage(NO_COMPILER_FOUND_TEXT, compilerType);
            return Optional.empty();
        }

        Optional<String> aasm = Optional.empty();

        try
        {
            aasm = Optional.of(compiler.compile(scanner));
        } catch (CompilerException e)
        {
            final Style hoverEvent = Style.EMPTY.withHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, Text.literal(AutokeyException.prettyPrint(e))));
            Feedback.sendClientErrorMessage(COMPILE_ERROR_TEXT.setStyle(hoverEvent), e, name);
        }

        scanner.close();

        return aasm;
    }
}
