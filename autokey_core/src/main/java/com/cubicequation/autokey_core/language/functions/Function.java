package com.cubicequation.autokey_core.language.functions;

import org.jetbrains.annotations.NotNull;

import java.util.Optional;

public class Function
{
    protected final Overload[] overloads;

    public Function(final @NotNull Overload @NotNull [] overloads)
    {
        this.overloads = overloads;
    }

    public final @NotNull Optional<Overload> getOverload(final Class<?> @NotNull [] argumentTypes)
    {
        Optional<Overload> candidate = Optional.empty();

        for (Overload o : overloads)
        {
            final Optional<Boolean> match = o.matches(argumentTypes);
            if (match.isEmpty()) continue;

            if (match.get()) return Optional.of(o);
            else if (candidate.isEmpty()) candidate = Optional.of(o);
        }

        return candidate;
    }
}
