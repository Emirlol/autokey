package com.cubicequation.autokey_core.language.aasm.v0;

record Token(TokenType type, String value, int line, int start, int length)
{
}
