package com.cubicequation.autokey_core.util;

public final class Ref<T>
{
    private T value;

    public Ref(T value)
    {
        this.value = value;
    }

    public void setValue(T value)
    {
        this.value = value;
    }

    public T getValue()
    {
        return value;
    }
}
