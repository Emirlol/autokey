package com.cubicequation.autokey_core.util;

import com.mojang.logging.LogUtils;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.hud.InGameHud;
import net.minecraft.text.Text;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;

public final class Feedback
{
    private static final String INGAME_HUD_NULL = "Could not send chat message because InGameHud is null.\nMessage: ";
    private static final Logger LOGGER = LogUtils.getLogger();

    private Feedback()
    {
        throw new IllegalStateException("Feedback cannot be instantiated");
    }

    public static String sendClientMessage(final Text message)
    {
        final InGameHud hud = MinecraftClient.getInstance().inGameHud;

        if (hud == null)
        {
            final String error = INGAME_HUD_NULL + message.getString();
            LOGGER.error(error);
            return error;
        }

        hud.getChatHud()
                .addMessage(message);
        return message.getString();
    }

    public static String sendClientMessage(final @NotNull Text message, final Object... args)
    {
        final InGameHud hud = MinecraftClient.getInstance().inGameHud;

        if (hud == null)
        {
            final String error = INGAME_HUD_NULL + message.getString();
            LOGGER.error(error);
            return error;
        }

        final String text = String.format(message.getString()
                .replace('€', '%'), args);
        hud.getChatHud()
                .addMessage(Text.literal(text)
                        .setStyle(message.getStyle()));

        return text;
    }

    public static @NotNull String sendClientMessage(final String message)
    {
        final InGameHud hud = MinecraftClient.getInstance().inGameHud;

        if (hud == null)
        {
            final String error = INGAME_HUD_NULL + message;
            LOGGER.error(error);
            return error;
        }

        hud.getChatHud()
                .addMessage(Text.literal(message));
        return message;
    }

    public static @NotNull String sendClientMessage(final String message, final Object... args)
    {
        final InGameHud hud = MinecraftClient.getInstance().inGameHud;

        if (hud == null)
        {
            final String error = INGAME_HUD_NULL + message;
            LOGGER.error(error);
            return error;
        }

        final String text = String.format(message.replace('€', '%'), args);
        hud.getChatHud()
                .addMessage(Text.literal(text));

        return text;
    }

    public static void sendClientErrorMessage(final String message, final Throwable throwable)
    {
        final String text = Feedback.sendClientMessage(message);
        LOGGER.error(text, throwable);
    }

    public static void sendClientErrorMessage(final String message, final Throwable throwable, final Object... args)
    {
        final String text = Feedback.sendClientMessage(message, args);
        LOGGER.error(text, throwable);
    }

    public static void sendClientErrorMessage(final Text message, final Throwable throwable)
    {
        final String text = Feedback.sendClientMessage(message);
        LOGGER.error(text, throwable);
    }

    public static void sendClientErrorMessage(final Text message, final Throwable throwable, final Object... args)
    {
        final String text = Feedback.sendClientMessage(message, args);
        LOGGER.error(text, throwable);
    }
}
