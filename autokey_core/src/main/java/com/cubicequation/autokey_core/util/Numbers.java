package com.cubicequation.autokey_core.util;

import java.util.Optional;

public final class Numbers
{
    private Numbers()
    {
        throw new IllegalStateException("Numbers cannot be instantiated");
    }

    public static Optional<Integer> parseInt(final String value)
    {
        try
        {
            return  Optional.of(Integer.parseInt(value));
        }
        catch (NumberFormatException ignored)
        {
            return Optional.empty();
        }
    }

    public static Optional<Float> parseFloat(final String value)
    {
        try
        {
            return Optional.of(Float.parseFloat(value));
        }
        catch (NumberFormatException ignored)
        {
            return Optional.empty();
        }
    }

    public static Optional<Double> parseDouble(final String value)
    {
        try
        {
            return Optional.of(Double.parseDouble(value));
        }
        catch (NumberFormatException ignored)
        {
            return Optional.empty();
        }
    }
}
