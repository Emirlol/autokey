package com.cubicequation.autokey_core.util;

import com.mojang.logging.LogUtils;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

public final class TickScheduler
{
    private static final ArrayList<TickScheduleable> TICKSCHEDULEABLES = new ArrayList<>();
    private static final int DEFAULT_DELAY = 1;
    private static final int DEFAULT_LOOP_DELAY = 0;

    private TickScheduler()
    {
        throw new IllegalStateException("TickScheduler cannot be instantiated");
    }

    public static void tick()
    {
        final ArrayList<TickScheduleable> toRun = new ArrayList<>();

        for (TickScheduleable tickScheduleable : TICKSCHEDULEABLES)
        {
            if (tickScheduleable.latch.getCount() > 1)
            {
                tickScheduleable.latch.countDown();
            }
            else
            {
                toRun.add(tickScheduleable);
            }
        }

        final ArrayList<TickScheduleable> toRemove = new ArrayList<>();
        for (TickScheduleable tickScheduleable : toRun)
        {
            try
            {
                if (tickScheduleable.async)
                {
                    tickScheduleable.thread = new Thread(tickScheduleable::execute);
                    tickScheduleable.thread.start();
                }
                else tickScheduleable.execute();
            } catch (Exception e)
            {
                LogUtils.getLogger()
                        .error("An error was thrown while trying to execute a tickScheduleable.", e);
            }

            tickScheduleable.latch.countDown();

            if (tickScheduleable.loopDelay > 0) tickScheduleable.latch = new CountDownLatch(tickScheduleable.loopDelay);
            else toRemove.add(tickScheduleable);
        }

        TICKSCHEDULEABLES.removeAll(toRemove);
    }

    public static @NotNull TickScheduleable schedule(final TickScheduleable.ScheduleableRunnable runnable)
    {
        final TickScheduleable scheduleable = new TickScheduleable(runnable, false, DEFAULT_DELAY, DEFAULT_LOOP_DELAY);
        TICKSCHEDULEABLES.add(scheduleable);
        return scheduleable;
    }

    public static @NotNull TickScheduleable schedule(final TickScheduleable.ScheduleableRunnable runnable, final boolean async)
    {
        final TickScheduleable scheduleable = new TickScheduleable(runnable, async, DEFAULT_DELAY, DEFAULT_LOOP_DELAY);
        TICKSCHEDULEABLES.add(scheduleable);
        return scheduleable;
    }

    public static @NotNull TickScheduleable schedule(final TickScheduleable.ScheduleableRunnable runnable, final int delay)
    {
        final TickScheduleable scheduleable = new TickScheduleable(runnable, false, delay, DEFAULT_LOOP_DELAY);
        TICKSCHEDULEABLES.add(scheduleable);
        return scheduleable;
    }

    public static @NotNull TickScheduleable schedule(final TickScheduleable.ScheduleableRunnable runnable, final boolean async, final int delay)
    {
        final TickScheduleable scheduleable = new TickScheduleable(runnable, async, delay, DEFAULT_LOOP_DELAY);
        TICKSCHEDULEABLES.add(scheduleable);
        return scheduleable;
    }

    public static @NotNull TickScheduleable schedule(final TickScheduleable.ScheduleableRunnable runnable, final int delay, final int loopDelay)
    {
        final TickScheduleable scheduleable = new TickScheduleable(runnable, false, delay, loopDelay);
        TICKSCHEDULEABLES.add(scheduleable);
        return scheduleable;
    }

    public static @NotNull TickScheduleable schedule(final TickScheduleable.ScheduleableRunnable runnable, final boolean async, final int delay, final int loopDelay)
    {
        final TickScheduleable scheduleable = new TickScheduleable(runnable, async, delay, loopDelay);
        TICKSCHEDULEABLES.add(scheduleable);
        return scheduleable;
    }

    public static boolean contains(final TickScheduleable tickScheduleable)
    {
        return TICKSCHEDULEABLES.contains(tickScheduleable);
    }

    public static void unSchedule(final TickScheduleable scheduleable)
    {
        TICKSCHEDULEABLES.remove(scheduleable);
    }

    public static final class TickScheduleable
    {
        private final int loopDelay;
        private final boolean async;
        private final ScheduleableRunnable runnable;
        private CountDownLatch latch;
        private Thread thread;
        private boolean exited;

        private TickScheduleable(final ScheduleableRunnable runnable, final boolean async, final int delay, final int loopDelay)
        {
            this.runnable = runnable;
            this.async = async;
            this.loopDelay = Math.max(loopDelay, 0);
            latch = new CountDownLatch(Math.max(delay, 1));
            exited = false;
        }

        private void execute()
        {
            runnable.run(this);
            exited = true;
        }

        public boolean isAsync()
        {
            return async;
        }

        public boolean hasExited()
        {
            return exited;
        }

        public void interrupt()
        {
            exited = true;
            thread.interrupt();
        }

        public void get() throws InterruptedException
        {
            latch.await();
        }

        public interface ScheduleableRunnable
        {
            void run(TickScheduleable tickScheduleable);
        }
    }
}
