package com.cubicequation.autokey_core.file;

import com.cubicequation.autokey_core.util.Feedback;
import net.minecraft.client.MinecraftClient;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import org.jetbrains.annotations.NotNull;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Scanner;

public final class PropertyManager
{
    private static final HashMap<String, String> DEFAULT_PROPERTIES = new HashMap<>();
    private static final MutableText SAVE_ERROR_TEXT = Text.translatable("autokey_core.file.properties.save_failed");
    private static final MutableText LOAD_ERROR_TEXT = Text.translatable("autokey_core.file.properties.load_failed");
    private final HashMap<String, String> properties;
    private final File file;

    PropertyManager(final @NotNull File file)
    {
        properties = new HashMap<>();
        this.file = file;
    }

    public static void registerDefaultProperty(final String propertyName, final String propertyValue)
    {
        DEFAULT_PROPERTIES.put(propertyName, propertyValue);
    }

    public @NotNull String getProperty(final String propertyName)
    {
        final String propertyValue = properties.get(propertyName);
        return propertyValue == null ? "" : propertyValue;
    }

    public void setProperty(final String propertyName, final String propertyValue)
    {
        properties.put(propertyName, propertyValue);
    }

    public boolean save()
    {
        StringBuilder sb = new StringBuilder();

        properties.forEach((name, value) -> sb
                .append(name)
                .append('=')
                .append(value)
                .append(System.lineSeparator()));

        final java.io.File propertiesFile = new java.io.File(file.getFile(), "properties");

        try
        {
            Files.write(propertiesFile.toPath(), sb.toString()
                    .getBytes());
        } catch (IOException e)
        {
            Feedback.sendClientErrorMessage(SAVE_ERROR_TEXT, e, file.getName());
            return false;
        }

        return true;
    }

    public boolean load()
    {
        final java.io.File propertiesFile = new java.io.File(file.getFile(), "properties");

        if (!propertiesFile.isFile())
        {
            properties.clear();
            properties.putAll(DEFAULT_PROPERTIES);
            return false;
        }

        Scanner scanner;
        try
        {
            scanner = new Scanner(propertiesFile);
        } catch (FileNotFoundException e)
        {
            if (MinecraftClient.getInstance().world == null)
                Feedback.sendClientErrorMessage("§4§oCould not load the properties of '" + file.getName() + "'.", e);
            else Feedback.sendClientErrorMessage(LOAD_ERROR_TEXT, e, file.getName());
            return false;
        }

        final HashMap<String, String> newProperties = new HashMap<>();

        while (scanner.hasNextLine())
        {
            final String[] parts = scanner.nextLine()
                    .split("=");

            if (parts.length != 2) continue;

            newProperties.put(parts[0], parts[1]);
        }

        scanner.close();

        properties.clear();
        properties.putAll(DEFAULT_PROPERTIES);
        properties.putAll(newProperties);

        return true;
    }
}
