package com.cubicequation.autokey_core.file;

import com.cubicequation.autokey_core.entrypoints.Autokey;
import com.cubicequation.autokey_core.language.Languages;
import com.cubicequation.autokey_core.util.Feedback;
import com.cubicequation.autokey_core.util.Numbers;
import com.cubicequation.autokey_core.util.TickScheduler;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import org.jetbrains.annotations.NotNull;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;

public final class File
{
    private static final java.io.File DIRECTORY = new java.io.File(FabricLoader.getInstance()
            .getGameDir()
            .toString(), "autokey");
    private static final MutableText COMPILING_TEXT = Text.translatable("autokey_core.file.compile_start");
    private static final MutableText NO_COMPILER_TEXT = Text.translatable("autokey_core.file.no_compiler_selected");
    private static final MutableText FILE_DOES_NOT_EXIST_TEXT = Text.translatable("autokey_core.io.file_missing");
    private static final MutableText READ_ERROR_TEXT = Text.translatable("autokey_core.io.read_error");
    private static final MutableText WRITE_ERROR_TEXT = Text.translatable("autokey_core.io.write_error");
    private static final MutableText COMPILE_SUCCESS_TEXT = Text.translatable("autokey_core.file.compile_success");
    private static File[] FILES = new File[0];

    static
    {
        PropertyManager.registerDefaultProperty("compiler", "");
        PropertyManager.registerDefaultProperty("chatSubscribed", "false");
        PropertyManager.registerDefaultProperty("chatPrefix", "");
        PropertyManager.registerDefaultProperty("actionKey", "0");
        PropertyManager.registerDefaultProperty("onlyAllowClientMessages", "true");
    }

    private final String name;
    private final String[] languages;
    private final java.io.File file;
    private final UIManager uiManager;
    private final StateManager stateManager;
    private final ExecutionManager executionManager;
    private final PropertyManager propertyManager;

    private File(final @NotNull java.io.File directory, final @NotNull String @NotNull [] languages)
    {
        this.file = directory;
        this.name = directory.getName();
        this.languages = languages;

        uiManager = new UIManager();
        stateManager = new StateManager(this);
        executionManager = new ExecutionManager(this);
        propertyManager = new PropertyManager(this);

        getStateManager().updateCompilingState();
        getStateManager().updateExecutionState();

        propertyManager.load();

        final Optional<Integer> id = Numbers.parseInt(getPropertyManager().getProperty("actionKey")
                .trim());
        if (id.isPresent() && id.get() > 0 && id.get() < 10)
            Autokey.setAction(id.get() - 1, this);

        if (Objects.equals(getPropertyManager().getProperty("compiler"), "") && languages.length != 0)
            getPropertyManager().setProperty("compiler", languages[0]);
    }

    public static java.io.File getDirectory()
    {
        if (DIRECTORY.isFile()) DIRECTORY.delete();
        DIRECTORY.mkdir();
        return DIRECTORY;
    }

    public static File[] getFiles()
    {
        return FILES;
    }

    public static void updateFiles()
    {
        ExecutionManager.cancelAll();

        final java.io.File[] scripts = getDirectory().listFiles(java.io.File::isDirectory);

        if (scripts == null)
        {
            FILES = new File[0];
            return;
        }

        final ArrayList<File> newFiles = new ArrayList<>();

        for (java.io.File file : scripts)
        {
            final java.io.File[] contentFiles = file.listFiles(java.io.File::isFile);

            if (contentFiles == null)
            {
                FILES = new File[0];
                return;
            }

            final String[] languages = Arrays.stream(contentFiles)
                    .map(java.io.File::getName)
                    .filter(name ->
                            !name.isEmpty() &&
                                    !name.equals("aasm"))
                    .toArray(String[]::new);

            newFiles.add(new File(file, languages));
        }

        File.FILES = newFiles.toArray(File[]::new);
    }

    public @NotNull String getName()
    {
        return name;
    }

    public String[] getLanguages()
    {
        return languages;
    }

    public java.io.@NotNull File getFile()
    {
        if (!file.isDirectory()) file.delete();
        file.mkdir();
        return file;
    }

    public UIManager getUIManager()
    {
        return uiManager;
    }

    public StateManager getStateManager()
    {
        return stateManager;
    }

    public ExecutionManager getExecutionManager()
    {
        return executionManager;
    }

    public PropertyManager getPropertyManager()
    {
        return propertyManager;
    }

    public void compile()
    {
        Feedback.sendClientMessage(COMPILING_TEXT, name);

        getStateManager().setCompiling();
        getStateManager().updateExecutionState();

        final String currentCompiler = getPropertyManager().getProperty("compiler");
        if (Objects.equals(currentCompiler, ""))
        {
            Feedback.sendClientMessage(NO_COMPILER_TEXT);
            getStateManager().updateCompilingState();
            return;
        }

        final java.io.File file = new java.io.File(getFile(), currentCompiler);
        if (!file.isFile())
        {
            Feedback.sendClientMessage(FILE_DOES_NOT_EXIST_TEXT, currentCompiler);
            stateManager.updateCompilingState();
            return;
        }

        Scanner scanner;
        try
        {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e)
        {
            Feedback.sendClientErrorMessage(READ_ERROR_TEXT, e, currentCompiler);
            getStateManager().updateCompilingState();
            return;
        }

        TickScheduler.schedule(runnable ->
        {
            final Optional<String> aasm = Languages.compile(currentCompiler, getName(), scanner);

            if (aasm.isEmpty())
            {
                getStateManager().updateCompilingState();
                getStateManager().updateExecutionState();
                return;
            }

            java.io.File aasmFile = new java.io.File(getFile(), "aasm");

            try
            {
                Files.write(aasmFile.toPath(), aasm.get()
                        .getBytes());
            } catch (IOException e)
            {
                Feedback.sendClientErrorMessage(WRITE_ERROR_TEXT, e, currentCompiler);
                getStateManager().updateCompilingState();
                return;
            }

            Feedback.sendClientMessage(COMPILE_SUCCESS_TEXT, getName());
            getStateManager().updateCompilingState();
            getStateManager().updateExecutionState();
        }, true);
    }
}