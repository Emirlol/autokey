package com.cubicequation.autokey_core.file;

import com.cubicequation.autokey_core.language.aasm.AasmVM;
import com.cubicequation.autokey_core.language.aasm.Environment;
import com.cubicequation.autokey_core.util.Feedback;
import com.cubicequation.autokey_core.util.TickScheduler;
import net.minecraft.client.MinecraftClient;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Objects;
import java.util.Scanner;

public final class ExecutionManager
{
    private static final MutableText NOT_COMPILED_TEXT = Text.translatable("autokey_core.file.not_compiled");
    private static final MutableText READ_ERROR_TEXT = Text.translatable("autokey_core.io.read_error");
    private final File file;
    private AasmVM vm;
    ExecutionManager(final @NotNull File file)
    {
        this.file = file;
    }

    public static void cancelAll()
    {
        for (File file : File.getFiles())
        {
            if (file.getStateManager()
                    .getExecutionState() != ExecutionState.CANCELABLE) continue;
            file.getExecutionManager()
                    .cancel();
        }
    }

    public static boolean receivedClientMessage(final @NotNull String chatMessage)
    {
        boolean suppressMessage = false;

        for (File file : File.getFiles())
        {
            final PropertyManager propertyManager = file.getPropertyManager();
            if (file.getStateManager()
                    .getExecutionState() != ExecutionState.EXECUTABLE ||
                    Objects.equals(propertyManager.getProperty("chatSubscribed"), "false") ||
                    Objects.equals(propertyManager.getProperty("onlyAllowClientMessages"), "false") ||
                    !chatMessage.startsWith(propertyManager.getProperty("chatPrefix")))
                continue;

            final HashMap<String, String> environment = new HashMap<>();
            environment.put("chatMessage", chatMessage);

            file.getExecutionManager()
                    .execute(true, new Environment(environment));

            suppressMessage = true;
        }

        return suppressMessage;
    }

    public static void receivedChatMessage(final @NotNull String chatMessage)
    {
        for (File file : File.getFiles())
        {
            final PropertyManager propertyManager = file.getPropertyManager();
            if (file.getStateManager()
                    .getExecutionState() != ExecutionState.EXECUTABLE ||
                    Objects.equals(propertyManager.getProperty("chatSubscribed"), "false") ||
                    Objects.equals(propertyManager.getProperty("onlyAllowClientMessages"), "true") ||
                    !chatMessage.startsWith(propertyManager.getProperty("chatPrefix"))) continue;

            final HashMap<String, String> environment = new HashMap<>();
            environment.put("chatMessage", chatMessage);

            file.getExecutionManager()
                    .execute(true, new Environment(environment));
        }
    }

    public void initialize(final boolean silent)
    {
        final java.io.File asmFile = new java.io.File(file.getFile(), "aasm");
        if (!(asmFile.isFile() || silent)) Feedback.sendClientMessage(NOT_COMPILED_TEXT, file.getName());

        Scanner scanner;
        try
        {
            scanner = new Scanner(asmFile);
        } catch (FileNotFoundException e)
        {
            Feedback.sendClientErrorMessage(READ_ERROR_TEXT, e, file.getName());
            return;
        }

        MinecraftClient.getInstance()
                .setScreen(null);

        vm = new AasmVM(scanner, file.getName());
    }

    public void execute(final boolean silent, final @Nullable Environment environment)
    {
        initialize(silent);
        vm.run(silent, environment, () -> TickScheduler.schedule(__ -> file.getStateManager()
                .updateExecutionState(), true));
        file.getStateManager()
                .updateExecutionState();
    }

    public void execute()
    {
        execute(false, null);
    }

    public void cancel()
    {
        vm.interrupt();
        file.getStateManager()
                .updateExecutionState();
    }

    boolean isExecuting()
    {
        return vm != null && vm.isRunning();
    }
}
