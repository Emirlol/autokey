package com.cubicequation.autokey_core.file;

public enum CompilingState
{
    UN_COMPILED,
    COMPILING,
    COMPILED
}
