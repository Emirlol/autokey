package com.cubicequation.autokey_core.file;

import com.cubicequation.autokey_core.gui.MenuScreen;
import com.cubicequation.autokey_core.gui.SettingsScreen;
import net.minecraft.client.MinecraftClient;

public final class UIManager
{
    private Runnable menuEntryCallback;

    public void setMenuEntryCallback(Runnable callback)
    {
        this.menuEntryCallback = callback;
    }

    void invokeMenuEntryCallback()
    {
        if (menuEntryCallback != null && MinecraftClient.getInstance().currentScreen instanceof MenuScreen)
            menuEntryCallback.run();
    }


    private Runnable settingsScreenCallback;

    public void setSettingsScreenCallback(Runnable callback)
    {
        this.settingsScreenCallback = callback;
    }

    void invokeSettingsScreenCallback()
    {
        if (settingsScreenCallback != null && MinecraftClient.getInstance().currentScreen instanceof SettingsScreen)
            settingsScreenCallback.run();
    }
}
