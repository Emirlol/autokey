package com.cubicequation.autokey_core.gui;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.Drawable;
import net.minecraft.client.gui.Element;
import net.minecraft.client.sound.PositionedSoundInstance;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.Identifier;
import org.jetbrains.annotations.NotNull;

public class CustomToggleButtonWidget implements Drawable, Element
{
    private static final Identifier TEXTURE;

    static
    {
        TEXTURE = new Identifier("autokey_core:textures/gui/toggle_button.png");
    }

    private final int x;
    private final int y;
    private final ClickAction clickAction;
    private boolean toggled;
    private boolean focused = false;

    public CustomToggleButtonWidget(int x, int y, boolean toggled, ClickAction clickAction)
    {
        this.x = x;
        this.y = y;

        this.clickAction = clickAction;
        this.toggled = toggled;
    }

    public void render(@NotNull DrawContext context, int mouseX, int mouseY, float delta)
    {
        final int textureX = isMouseOver(mouseX, mouseY) ? 12 : 0;
        final int textureY = toggled ? 12 : 0;

        context.drawTexture(TEXTURE, x, y, textureX, textureY, 12, 12, 36, 24);
    }

    public boolean mouseClicked(double mouseX, double mouseY, int button)
    {
        if (button == 0 && isMouseOver(mouseX, mouseY))
        {
            MinecraftClient.getInstance()
                    .getSoundManager()
                    .play(PositionedSoundInstance.master(SoundEvents.UI_BUTTON_CLICK, 1.0F));
            toggled = !toggled;
            clickAction.click(toggled);
            return true;
        }

        return false;
    }

    public boolean isMouseOver(double mouseX, double mouseY)
    {
        return mouseX >= x && mouseY >= y && mouseX < x + 12 && mouseY < y + 12;
    }

    @Override
    public boolean isFocused()
    {
        return this.focused;
    }

    @Override
    public void setFocused(boolean focused)
    {
        this.focused = focused;
    }

    public void setToggled(boolean value)
    {
        toggled = value;
    }

    public interface ClickAction
    {
        void click(boolean toggled);
    }
}
