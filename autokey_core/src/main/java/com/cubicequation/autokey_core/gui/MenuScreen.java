package com.cubicequation.autokey_core.gui;

import com.cubicequation.autokey_core.entrypoints.Autokey;
import com.cubicequation.autokey_core.file.File;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;
import org.jetbrains.annotations.NotNull;
import org.lwjgl.glfw.GLFW;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.stream.Stream;

public class MenuScreen extends Screen
{
    private static final Identifier TEXTURE;
    private static final Text SEARCH_HINT_TEXT;
    private static final int BACKGROUND_WIDTH;
    private static final int BACKGROUND_HEIGHT;
    private static final int ENTRY_PADDING;
    private static final MutableText SEARCH_FIELD_DEFAULT_TEXT;

    private static File[] foundFiles;
    private static String searchText;

    static
    {
        TEXTURE = new Identifier("autokey_core:textures/gui/menu.png");

        BACKGROUND_WIDTH = 147;
        BACKGROUND_HEIGHT = 168;

        SEARCH_HINT_TEXT = Text.translatable("gui.autokey_core.menu.search_hint")
                .formatted(Formatting.ITALIC)
                .formatted(Formatting.GRAY);
        SEARCH_FIELD_DEFAULT_TEXT = Text.translatable("itemGroup.search");
        searchText = "";

        ENTRY_PADDING = 19;
    }

    private CustomButtonWidget openFolderButton;
    private CustomButtonWidget refreshSearchButton;
    private CustomButtonWidget resetSearchField;
    private CustomHoldButtonWidget scrollButton;
    private TextFieldWidget searchField;
    private MenuEntry[] menuEntries;
    private boolean searching;
    private int x;
    private int y;
    private int entryX;
    private int entryY;

    public MenuScreen()
    {
        super(Text.translatable("gui.autokey_core.menu_screen.title"));

        menuEntries = new MenuEntry[0];

        setupSearchResults();
    }

    protected void init()
    {
        x = (width - BACKGROUND_WIDTH) / 2;
        y = (height - BACKGROUND_HEIGHT) / 2;
        entryX = x + 10;
        entryY = y + 27;

        searchField = new TextFieldWidget(MinecraftClient.getInstance().textRenderer, x + 27, y + 13, 75, 14, SEARCH_FIELD_DEFAULT_TEXT);
        searchField.setMaxLength(50);
        searchField.setDrawsBackground(false);
        searchField.setText(searchText);

        openFolderButton = new CustomButtonWidget(x + 123, y + 10, TEXTURE, 0, 168, 14, 168, -1, -1, 14, Autokey::openDirectory);

        refreshSearchButton = new CustomButtonWidget(x + 107, y + 10, TEXTURE, 0, 182, 14, 182, -1, -1, 14, this::reFetchSearchResults);

        resetSearchField = new CustomButtonWidget(x + 11, y + 11, TEXTURE, 0, 196, 12, 196, -1, -1, 12, () ->
        {
            searchField.setText("");
            refreshSearchResults();
        });

        scrollButton = new CustomHoldButtonWidget(x + 130, y + 28, TEXTURE, 0, 208, 12, 208, 6, 15, 129);

        refreshEntries();
    }

    public boolean mouseClicked(double mouseX, double mouseY, int button)
    {
        if (searchField.mouseClicked(mouseX, mouseY, button))
        {
            searchField.setFocused(true);
            return true;
        }
        else searchField.setFocused(false);

        if (openFolderButton.mouseClicked(mouseX, mouseY, button))
        {
            openFolderButton.setFocused(true);
            return true;
        }
        else openFolderButton.setFocused(false);

        if (refreshSearchButton.mouseClicked(mouseX, mouseY, button))
        {
            refreshSearchButton.setFocused(true);
            return true;
        }
        else refreshSearchButton.setFocused(false);

        if (resetSearchField.mouseClicked(mouseX, mouseY, button))
        {
            resetSearchField.setFocused(true);
            return true;
        }
        else resetSearchField.setFocused(false);

        if (scrollButton.mouseClicked(mouseX, mouseY, button))
        {
            scrollButton.setFocused(true);
            return true;
        }
        else scrollButton.setFocused(false);

        for (MenuEntry entry : menuEntries)
            if (entry.mouseClicked(mouseX, mouseY, button))
            {
                entry.setFocused(true);
                return true;
            }
            else entry.setFocused(false);

        return false;
    }

    public boolean mouseReleased(double mouseX, double mouseY, int button)
    {
        if (scrollButton.mouseReleased(mouseX, mouseY, button))
        {
            scrollButton.setFocused(true);
            return true;
        }
        else scrollButton.setFocused(false);

        return false;
    }

    public boolean mouseScrolled(double mouseX, double mouseY, double amount)
    {
        if (scrollButton.mouseScrolled(mouseX, mouseY, amount))
        {
            scrollButton.setFocused(true);
            return true;
        }
        else scrollButton.setFocused(false);

        return false;
    }

    public boolean charTyped(char chr, int modifiers)
    {
        if (searching) return false;
        else if (searchField.charTyped(chr, modifiers))
        {
            refreshSearchResults();
            searchField.setFocused(true);
            return true;
        }
        else searchField.setFocused(false);

        return false;
    }

    public boolean keyPressed(int keyCode, int scanCode, int modifiers)
    {
        searching = false;

        if (keyCode == GLFW.GLFW_KEY_ESCAPE)
        {
            MinecraftClient.getInstance()
                    .setScreen(null);
            return true;
        }

        if (searchField.keyPressed(keyCode, scanCode, modifiers))
        {
            refreshSearchResults();
            searchField.setFocused(true);
            return true;
        }
        else if (searchField.isFocused() && searchField.isVisible())
        {
            scrollButton.setFocused(false);
            return true;
        }
        else
        {
            scrollButton.setFocused(false);
            assert client != null;
            if (client.options.chatKey.matchesKey(keyCode, scanCode) && !searchField.isFocused())
            {
                searching = true;
                searchField.setFocused(true);
                return true;
            }
        }

        return false;
    }

    public boolean keyReleased(int keyCode, int scanCode, int modifiers)
    {
        searching = false;
        return false;
    }

    public void render(DrawContext context, int mouseX, int mouseY, float delta)
    {
        //Render dark background
        this.renderBackground(context, mouseX, mouseY, delta);

        super.render(context, mouseX, mouseY, delta);

        //Render UI background
        drawBackground(context);

        //searchField
        if (!searchField.isFocused() && searchField.getText()
                .isEmpty())
            context.drawTextWithShadow(MinecraftClient.getInstance().textRenderer, SEARCH_HINT_TEXT, x + 27, y + 13, -1);
        else searchField.render(context, mouseX, mouseY, delta);

        //Buttons
        openFolderButton.render(context, mouseX, mouseY, delta);
        refreshSearchButton.render(context, mouseX, mouseY, delta);
        resetSearchField.render(context, mouseX, mouseY, delta);

        scrollButton.render(context, mouseX, mouseY, delta);
        if (scrollButton.percentageChanged()) refreshEntries();

        //Render search results
        for (MenuEntry entry : menuEntries)
            entry.render(context, mouseX, mouseY, delta);
    }

    private void drawBackground(@NotNull DrawContext context)
    {
        context.drawTexture(TEXTURE, x, y, 0, 0, BACKGROUND_WIDTH, BACKGROUND_HEIGHT);
    }

    private void setupSearchResults()
    {
        if (File.getFiles().length == 0) File.updateFiles();

        File[] files = File.getFiles();

        Stream<File> arrayStream = Arrays.stream(files);
        Stream<File> filteredStream = arrayStream.filter(f ->
        {
            String fileName = f.getName();
            fileName = fileName.toLowerCase(Locale.ROOT);
            return fileName.contains(searchText);
        });

        files = filteredStream.toArray(File[]::new);

        foundFiles = files;
    }

    private void refreshSearchResults()
    {
        searchText = searchField.getText();
        searchText = searchText.toLowerCase(Locale.ROOT);
        searchText = searchText.trim();

        File[] files = File.getFiles();

        Stream<File> arrayStream = Arrays.stream(files);
        Stream<File> filteredStream = arrayStream.filter(f ->
        {
            String fileName = f.getName();
            fileName = fileName.toLowerCase(Locale.ROOT);
            return fileName.contains(searchText);
        });

        files = filteredStream.toArray(File[]::new);

        foundFiles = files;

        refreshEntries();
    }

    private void reFetchSearchResults()
    {
        File.updateFiles();
        refreshSearchResults();
    }

    private void refreshEntries()
    {
        ArrayList<MenuEntry> entries = new ArrayList<>();
        scrollButton.setScrollAmount(foundFiles.length - 7);

        final boolean needsScrolling = foundFiles.length > 7;

        scrollButton.enabled(needsScrolling);
        if (!needsScrolling) scrollButton.resetY();

        final int start = needsScrolling ? Math.round(scrollButton.getScrollPercentage() * (foundFiles.length - 7)) : 0;
        final int biggerThanI = needsScrolling ? 7 : foundFiles.length;

        for (int i = 0; i < biggerThanI; i++)
            entries.add(new MenuEntry(entryX, entryY + ENTRY_PADDING * i, foundFiles[start + i]));

        menuEntries = entries.toArray(new MenuEntry[0]);
    }
}
