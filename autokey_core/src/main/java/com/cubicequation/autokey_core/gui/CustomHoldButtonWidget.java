package com.cubicequation.autokey_core.gui;

import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.Drawable;
import net.minecraft.client.gui.Element;
import net.minecraft.util.Identifier;
import org.jetbrains.annotations.NotNull;

public class CustomHoldButtonWidget implements Drawable, Element
{
    private final int x;
    private int y;
    private final int finalY;
    private final Identifier texture;
    private final int textureX;
    private final int textureY;
    private final int textureSizeX;
    private final int textureSizeY;
    private final int disabledTextureX;
    private final int disabledTextureY;
    private final int scrollBarHeight;
    private boolean enabled;
    private boolean pressed;
    private float oldPercentage;
    private float scrollPercentage;
    private boolean focused = false;
    float scrollAmount;

    public CustomHoldButtonWidget(int x, int y, @NotNull Identifier texture, int textureX, int textureY, int disabledTextureX, int disabledTextureY, int textureSizeX, int textureSizeY, int scrollBarHeight)
    {
        this.x = x;
        this.y = y;
        finalY = y;

        this.texture = texture;
        this.textureX = textureX;
        this.textureY = textureY;
        this.textureSizeX = textureSizeX;
        this.textureSizeY = textureSizeY;
        this.disabledTextureX = disabledTextureX;
        this.disabledTextureY = disabledTextureY;
        this.scrollBarHeight = scrollBarHeight;
        this.enabled = true;

        pressed = false;
        oldPercentage = 0;
        scrollPercentage = 0;
        scrollAmount = 0;
    }

    public void render(DrawContext context, int mouseX, int mouseY, float delta)
    {
        if (pressed) updatePercentage(mouseY);

        y = Math.round((finalY + (scrollBarHeight - textureSizeY) * scrollPercentage));

        if (enabled || disabledTextureX == -1) context.drawTexture(texture, x, y, textureX, textureY, textureSizeX, textureSizeY);
        else context.drawTexture(texture, x, y, disabledTextureX, disabledTextureY, textureSizeX, textureSizeY);
    }

    public boolean mouseClicked(double mouseX, double mouseY, int button)
    {
        if (button == 0 && isMouseOver(mouseX, mouseY) && enabled)
        {
            pressed = true;
            return true;
        }

        return false;
    }

    public boolean mouseReleased(double mouseX, double mouseY, int button)
    {
        pressed = false;

        return button == 0 && isMouseOver(mouseX, mouseY) && enabled;
    }

    public boolean isMouseOver(double mouseX, double mouseY)
    {
        if (mouseX >= x && mouseY >= finalY && mouseX < x + textureSizeX && mouseY < finalY + scrollBarHeight)
        {
            return true;
        }

        pressed = false;
        return false;
    }

    @Override
    public void setFocused(boolean focused) {
        this.focused = focused;
    }

    @Override
    public boolean isFocused() {
        return this.focused;
    }

    public boolean mouseScrolled(double mouseX, double mouseY, double amount)
    {
        if (!enabled) return false;

        float distance = scrollPercentage - (float)amount / scrollAmount;
        distance = Math.min(distance, 1);
        distance = Math.max(distance, 0);

        scrollPercentage = distance;
        return true;
    }

    private void updatePercentage(int mouseY)
    {
        float mouseHeight = mouseY;
        mouseHeight = Math.min(mouseHeight, finalY + scrollBarHeight);
        mouseHeight = Math.max(mouseHeight, finalY);
        mouseHeight -= finalY;

        scrollPercentage = mouseHeight / scrollBarHeight;
    }

    public float getScrollPercentage()
    {
        return scrollPercentage;
    }

    public void enabled(boolean value)
    {
        enabled = value;
    }

    public void resetY()
    {
        y = finalY;
        scrollPercentage = 0;
    }

    public void setScrollAmount(float scrollAmount)
    {
        this.scrollAmount = scrollAmount;
    }

    public boolean percentageChanged()
    {
        if (oldPercentage != scrollPercentage)
        {
            oldPercentage = scrollPercentage;
            return true;
        }

        return false;
    }
}
