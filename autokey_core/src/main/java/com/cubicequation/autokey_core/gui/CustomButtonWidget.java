package com.cubicequation.autokey_core.gui;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.Drawable;
import net.minecraft.client.gui.Element;
import net.minecraft.client.sound.PositionedSoundInstance;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.Identifier;
import org.jetbrains.annotations.NotNull;

public class CustomButtonWidget implements Drawable, Element
{
    private final int x;
    private final int y;
    private final Identifier texture;
    private final int textureX;
    private final int textureY;
    private final int textureSize;
    private final int hoverTextureX;
    private final int hoverTextureY;
    private final int disabledTextureX;
    private final int disabledTextureY;
    private final Runnable clickAction;
    private boolean visibility;
    private boolean enabled;

    public CustomButtonWidget(int x, int y, @NotNull Identifier texture, int textureX, int textureY, int hoverTextureX, int hoverTextureY, int disabledTextureX, int disabledTextureY, int textureSize, Runnable clickAction)
    {
        this.x = x;
        this.y = y;

        this.texture = texture;
        this.textureX = textureX;
        this.textureY = textureY;
        this.textureSize = textureSize;
        this.hoverTextureX = hoverTextureX;
        this.hoverTextureY = hoverTextureY;
        this.disabledTextureX = disabledTextureX;
        this.disabledTextureY = disabledTextureY;
        this.clickAction = clickAction;
        this.enabled = true;

        visibility = true;
    }

    public void render(DrawContext context, int mouseX, int mouseY, float delta)
    {
        if (!visibility) return;

        if (!enabled && disabledTextureX != -1)
            context.drawTexture(texture, x, y, disabledTextureX, disabledTextureY, textureSize, textureSize);
        else if (isMouseOver(mouseX, mouseY) && hoverTextureX != -1)
            context.drawTexture(texture, x, y, hoverTextureX, hoverTextureY, textureSize, textureSize);
        else
            context.drawTexture(texture, x, y, textureX, textureY, textureSize, textureSize);
    }

    public boolean mouseClicked(double mouseX, double mouseY, int button)
    {
        if (button == 0 && isMouseOver(mouseX, mouseY) && visibility && enabled)
        {
            MinecraftClient.getInstance()
                    .getSoundManager()
                    .play(PositionedSoundInstance.master(SoundEvents.UI_BUTTON_CLICK, 1.0F));
            clickAction.run();
            return true;
        }

        return false;
    }

    public boolean isMouseOver(double mouseX, double mouseY)
    {
        return mouseX >= x && mouseY >= y && mouseX < x + textureSize && mouseY < y + textureSize;
    }

    @Override
    public boolean isFocused()
    {
        return false;
    }

    @Override
    public void setFocused(boolean focused)
    {

    }

    public void setVisibility(boolean value)
    {
        visibility = value;
    }

    public void enabled(boolean value)
    {
        enabled = value;
    }
}
