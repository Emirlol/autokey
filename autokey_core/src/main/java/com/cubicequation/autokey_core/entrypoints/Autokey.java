package com.cubicequation.autokey_core.entrypoints;

import com.cubicequation.autokey_core.api.AutokeyAPI;
import com.cubicequation.autokey_core.api.LoadAPI;
import com.cubicequation.autokey_core.file.ExecutionManager;
import com.cubicequation.autokey_core.file.File;
import com.cubicequation.autokey_core.gui.MenuScreen;
import com.cubicequation.autokey_core.language.Languages;
import com.cubicequation.autokey_core.language.Library;
import com.cubicequation.autokey_core.language.aasm.AasmVM;
import com.cubicequation.autokey_core.util.Feedback;
import com.cubicequation.autokey_core.util.TickScheduler;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.StringReader;
import com.mojang.brigadier.exceptions.SimpleCommandExceptionType;
import com.mojang.logging.LogUtils;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.command.v2.ClientCommandManager;
import net.fabricmc.fabric.api.client.command.v2.ClientCommandRegistrationCallback;
import net.fabricmc.fabric.api.client.command.v2.FabricClientCommandSource;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.fabricmc.loader.api.metadata.ModMetadata;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.option.KeyBinding;
import net.minecraft.client.util.InputUtil;
import net.minecraft.command.CommandRegistryAccess;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import net.minecraft.util.Util;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;

import java.util.Optional;
import java.util.Scanner;

@Environment(EnvType.CLIENT)
public final class Autokey implements ClientModInitializer
{
    private static final SimpleCommandExceptionType RUN_COMMAND_MISSING_EXCEPTION = new SimpleCommandExceptionType(Text.translatable("command.autokey_core.run.command_missing"));
    private static final MutableText RUN_COMMAND_SCRIPT_NAME_TEXT = Text.translatable("command.autokey_core.run.script_name");
    private static final KeyBinding[] ACTIONS = new KeyBinding[9];
    private static final File[] ACTION_FILES = new File[9];
    private static final Logger LOGGER = LogUtils.getLogger();
    private static KeyBinding OPEN_MENU_KEYBINDING;

    public Autokey()
    {
        OPEN_MENU_KEYBINDING = KeyBindingHelper.registerKeyBinding(new KeyBinding(
                "key.autokey_core.openMenu",
                InputUtil.Type.KEYSYM,
                InputUtil.GLFW_KEY_B,
                "category.autokey_core.category"));

        for (int i = 0; i < ACTIONS.length; i++)
            ACTIONS[i] = KeyBindingHelper.registerKeyBinding(new KeyBinding(
                    "key.autokey_core.action" + (i + 1),
                    InputUtil.Type.KEYSYM,
                    InputUtil.UNKNOWN_KEY.getCode(),
                    "category.autokey_core.category"));
    }

    private static void updateAll(@NotNull Autokey.MessageConsumer consumer)
    {
        ExecutionManager.cancelAll();
        Languages.updateLanguages(consumer);
        Library.updateFunctions(consumer);

        AutokeyAPI.iterateAPIs("autokey_core", LoadAPI.class, (LoadAPI api, ModMetadata metadata) -> {
            final String name = api.onLoad(consumer);
            consumer.consume("Loaded " + name + " from " + metadata.getName() + ".", MessageConsumer.Severity.INFO);
        });

        consumer.consume("Successfully reloaded Autokey!", MessageConsumer.Severity.INFO);
    }

    public static void setAction(final int index, final File file)
    {
        ACTION_FILES[index] = file;
    }

    public static void openDirectory()
    {
        Util.getOperatingSystem()
                .open(File.getDirectory());
    }

    @Override
    public void onInitializeClient()
    {
        ClientTickEvents.END_CLIENT_TICK.register(client ->
        {
            TickScheduler.tick();

            while (OPEN_MENU_KEYBINDING.wasPressed())
                TickScheduler.schedule(__ -> MinecraftClient.getInstance()
                        .setScreen(new MenuScreen()));

            for (int i = 0; i < ACTIONS.length; i++)
            {
                final File actionFile = ACTION_FILES[i];
                while (ACTIONS[i].wasPressed())
                    TickScheduler.schedule(__ ->
                    {
                        if (actionFile == null) return;

                        switch (actionFile.getStateManager()
                                .getExecutionState())
                        {
                            case EXECUTABLE -> actionFile.getExecutionManager()
                                    .execute(false, null);
                            case CANCELABLE -> actionFile.getExecutionManager()
                                    .cancel();
                        }
                    });
            }
        });

        TickScheduler.schedule(runnable -> updateAll(MessageConsumer.LOG), true);

        ClientCommandRegistrationCallback.EVENT.register(this::registerCommands);
    }

    private void registerCommands(final @NotNull CommandDispatcher<FabricClientCommandSource> dispatcher, final CommandRegistryAccess registry)
    {
        final var mainCommand = ClientCommandManager.literal("autokey");
        mainCommand.executes(context ->
        {
            TickScheduler.schedule(__ -> MinecraftClient.getInstance()
                    .setScreen(new MenuScreen()));
            return 1;
        });

        mainCommand.then(ClientCommandManager.literal("run")
                .executes(context ->
                {
                    throw RUN_COMMAND_MISSING_EXCEPTION.create();
                }));

        mainCommand.then(ClientCommandManager.literal("run")
                .then(ClientCommandManager.argument("command", StringReader::readString)
                        .executes(context ->
                        {
                            final String command = context.getArgument("command", String.class);
                            final String runCommandScriptName = RUN_COMMAND_SCRIPT_NAME_TEXT.getString();

                            final Optional<String> aasm = Languages.compile("akey", runCommandScriptName, new Scanner(command));

                            if (aasm.isEmpty()) return 1;

                            final AasmVM vm = new AasmVM(new Scanner(aasm.get()), runCommandScriptName);
                            vm.run(true, null, null);
                            return 1;
                        })));

        mainCommand.then(ClientCommandManager.literal("reload")
                .executes(context ->
                {
                    updateAll(((message, severity) ->
                    {
                        final char prefix = switch (severity)
                        {
                            case INFO -> '7';
                            case WARN -> '6';
                            case ERROR -> '4';
                        };

                        Feedback.sendClientMessage("§c§" + prefix + message);
                    }));
                    return 1;
                }));

        var mainRegistered = dispatcher.register(mainCommand);
        dispatcher.register(ClientCommandManager.literal("akey")
                .redirect(mainRegistered)
                .executes(context ->
                {
                    TickScheduler.schedule(__ -> MinecraftClient.getInstance()
                            .setScreen(new MenuScreen()));
                    return 1;
                }));
    }

    public interface MessageConsumer
    {
        MessageConsumer LOG = (message, severity) ->
        {
            switch (severity)
            {
                case INFO -> LOGGER.info(message);
                case WARN -> LOGGER.warn(message);
                case ERROR -> LOGGER.error(message);
            }
        };

        void consume(final @NotNull String message, final @NotNull Severity severity);

        enum Severity
        {
            INFO,
            WARN,
            ERROR
        }
    }
}