package com.cubicequation.autokey_core.mixins;

import com.cubicequation.autokey_core.file.ExecutionManager;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.Screen;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(MinecraftClient.class)
public abstract class MinecraftClientMixin
{
    @Inject(method = "disconnect(Lnet/minecraft/client/gui/screen/Screen;)V", at = @At("TAIL"))
    private void disconnect(Screen screen, CallbackInfo ci)
    {
        ExecutionManager.cancelAll();
    }

    /*
    @Inject(method = "<init>", at = @At("TAIL"))
    private void init(RunArgs args, CallbackInfo ci)
    {
        KeyBindings.updateKeyBindings(Autokey.MessageConsumer.LOG);
    }
    */
}
