package com.cubicequation.autokey_core.mixins;

import com.cubicequation.autokey_core.file.ExecutionManager;
import net.minecraft.client.gui.hud.ChatHud;
import net.minecraft.client.gui.hud.ChatHudLine;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(ChatHud.class)
public abstract class ChatHudMixin
{
    @Inject(method = "addMessage(Lnet/minecraft/client/gui/hud/ChatHudLine;)V", at = @At("TAIL"))
    private void addMessage(ChatHudLine message, CallbackInfo ci)
    {
        ExecutionManager.receivedChatMessage(message.content().getString());
    }
}
