package com.cubicequation.autokey_core.api;

import com.cubicequation.autokey_core.entrypoints.Autokey;

public interface LoadAPI
{
    String onLoad(Autokey.MessageConsumer consumer);
}
