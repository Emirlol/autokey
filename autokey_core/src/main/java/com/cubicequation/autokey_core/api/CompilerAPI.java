package com.cubicequation.autokey_core.api;

import com.cubicequation.autokey_core.language.Compiler;

import java.util.Map;

public interface CompilerAPI
{
    Map<String, ? extends Compiler> getCompilers();
}
