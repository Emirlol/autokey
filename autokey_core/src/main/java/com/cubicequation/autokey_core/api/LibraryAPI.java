package com.cubicequation.autokey_core.api;

import com.cubicequation.autokey_core.language.functions.Function;

import java.util.Map;

public interface LibraryAPI
{
    Map<String, ? extends Function> getFunctions();
}
