package com.cubicequation.autokey_core.api;

import com.cubicequation.autokey_core.entrypoints.Autokey;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.metadata.ModMetadata;
import org.jetbrains.annotations.NotNull;

public final class AutokeyAPI
{
    public static <T> void iterateAPIs(final String modID, final @NotNull Class<T> apiType, final @NotNull EntrypointAction<T> action)
    {
        FabricLoader.getInstance()
                .getEntrypointContainers(modID, Object.class)
                .forEach(entrypoint ->
                {
                    final ModMetadata metadata = entrypoint.getProvider()
                            .getMetadata();

                    final Object api = entrypoint.getEntrypoint();

                    if (apiType.isInstance(api)) action.run(apiType.cast(api), metadata);
                });
    }

    public interface EntrypointAction<T>
    {
        void run(final @NotNull T api, final @NotNull ModMetadata metadata);
    }
}
