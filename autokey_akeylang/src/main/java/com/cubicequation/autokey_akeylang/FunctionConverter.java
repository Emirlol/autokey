package com.cubicequation.autokey_akeylang;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public final class FunctionConverter
{
    private final Converter.LabelNumber labelNumber;
    private final ArrayList<Level> levels;
    private final String name;
    private int variableNumber;

    public FunctionConverter(final String name, final Converter.LabelNumber labelNumber)
    {
        this.labelNumber = labelNumber;
        variableNumber = 999;

        levels = new ArrayList<>();
        levels.add(new Level());
        this.name = name;
    }

    public @NotNull CharSequence get()
    {
        return levels.get(0).body.delete(0, 1);
    }

    private Level getCurrentLevel()
    {
        return levels.get(levels.size() - 1);
    }

    private @NotNull String getVariableName()
    {
        variableNumber++;
        return "variable$" + variableNumber;
    }

    @Contract("_ -> param1")
    private @NotNull StringBuilder newLine(final @NotNull StringBuilder sb)
    {
        return sb.append(System.getProperty("line.separator"));
    }

    private void previousStack(final @NotNull StringBuilder sb)
    {
        final String oldStackSize = getVariableName();
        final String newStackSize = getVariableName();
        newLine(sb)
                .append("var -1 ")
                .append(oldStackSize);
        newLine(sb)
                .append("stackAmount ")
                .append(oldStackSize);
        newLine(sb)
                .append("var -1 ")
                .append(newStackSize);
        newLine(sb)
                .append("subtract ")
                .append(oldStackSize)
                .append(" variable$1 >")
                .append(newStackSize);
        newLine(sb)
                .append("stack ")
                .append(newStackSize);
    }

    public boolean appendTree(final @NotNull Node root, final HashMap<String, FunctionConverter> functions) throws AKeyException
    {
        switch (root.getToken()
                .type())
        {
            case IF ->
            {
                final Level level = new Level();
                final String condition = getVariableName();
                final String target = getVariableName();
                final String targetLabel = labelNumber.getLabelName();

                levels.add(level);

                newLine(level.body)
                        .append("#Start of if condition")
                        .append(convertNode(root.getChild(0), functions));
                newLine(level.body)
                        .append("var false ")
                        .append(condition);
                newLine(level.body)
                        .append("invert ")
                        .append(root.getInputs()[0])
                        .append(" >")
                        .append(condition);

                newLine(level.body)
                        .append("#Target line");
                newLine(level.body)
                        .append("var \"")
                        .append(targetLabel)
                        .append("\" ")
                        .append(target);

                newLine(level.body)
                        .append("jmpif ")
                        .append(condition)
                        .append(" ")
                        .append(target);

                newLine(level.body)
                        .append("newStack");

                previousStack(level.tail);

                newLine(level.tail)
                        .append(targetLabel)
                        .append(':');

                newLine(level.tail)
                        .append("#End of if condition");
            }
            case WHILE ->
            {
                final Level level = new Level();
                final String condition = getVariableName();
                final String start = getVariableName();
                final String startLabel = labelNumber.getLabelName();
                final String end = getVariableName();
                final String endLabel = labelNumber.getLabelName();

                levels.add(level);

                newLine(level.body)
                        .append("#Start of while condition");

                newLine(level.body)
                        .append("var \"")
                        .append(startLabel)
                        .append("\" ")
                        .append(start);

                newLine(level.body)
                        .append(startLabel)
                        .append(':')
                        .append(convertNode(root.getChild(0), functions));

                newLine(level.body)
                        .append("var false ")
                        .append(condition);
                newLine(level.body)
                        .append("invert ")
                        .append(root.getInputs()[0])
                        .append(" >")
                        .append(condition);

                newLine(level.body)
                        .append("#Target line");
                newLine(level.body)
                        .append("var \"")
                        .append(endLabel)
                        .append("\" ")
                        .append(end);

                newLine(level.body)
                        .append("jmpif ")
                        .append(condition)
                        .append(" ")
                        .append(end);

                newLine(level.body)
                        .append("newStack");

                previousStack(level.tail);

                newLine(level.tail)
                        .append("jmp ")
                        .append(start);

                newLine(level.tail)
                        .append(endLabel)
                        .append(':');

                newLine(level.tail)
                        .append("#End of while condition");
            }
            case FUNCTION -> throw new AKeyException("autokey_akeylang.wrong_function_level", root.getToken());
            case CLOSE ->
            {
                final int parent = levels.size() - 2;
                final int current = levels.size() - 1;

                if (parent < 0) return true;

                final Level parentLevel = levels.get(parent);
                final Level currentLevel = levels.get(current);

                levels.remove(current);

                newLine(parentLevel.body);
                parentLevel.body
                        .append(currentLevel.body)
                        .append(currentLevel.tail);
                newLine(parentLevel.body);
            }
            case EQUALS ->
            {
                if (root.getChildCount() > 2)
                    throw AKeyException.unexpectedTokenException(root.getChild(2)
                            .getToken());
                else if (root.getChildCount() < 2)
                    throw AKeyException.missingTokenException(root.getChild(0)
                            .getToken());
                final Node variable = root.getChild(0);
                final Node value = root.getChild(1);
                final StringBuilder currentBody = getCurrentLevel().body;

                //Optimization
                if (value.getChildCount() == 0 && value.getType() == Node.Type.TYPE)
                {
                    newLine(currentBody)
                            .append(value.getToken()
                                    .value())
                            .append(' ')
                            .append(variable.getToken()
                                    .value());
                    break;
                }

                currentBody.append(convertNode(value, functions));
                newLine(currentBody);
                final String[] inputs = root.getInputs();

                if (inputs.length != 1) throw new AKeyException("autokey_akeylang.unknown_error", root.getToken());

                currentBody
                        .append("set ")
                        .append(inputs[0])
                        .append(' ')
                        .append(variable.getToken()
                                .value());
            }
            case RETURN ->
            {
                final StringBuilder sb = getCurrentLevel().body;

                newLine(sb)
                        .append("stackAmount variable$oldStackSize-")
                        .append(name);
                newLine(sb)
                        .append("subtract variable$oldStackSize-")
                        .append(name)
                        .append(" variable$1 >variable$newStackSize-")
                        .append(name);
                newLine(sb)
                        .append("stack variable$newStackSize-")
                        .append(name);

                newLine(sb)
                        .append("jmp variable$")
                        .append(name);
            }
            case VAR ->
            {
                final Node child = root.getChild(0);
                if (child.getChildCount() > 2)
                    throw AKeyException.unexpectedTokenException(child.getChild(2)
                            .getToken());
                else if (child.getChildCount() < 2)
                    throw AKeyException.missingTokenException(child.getChild(0)
                            .getToken());
                final Node variable = child.getChild(0);
                final Node value = child.getChild(1);
                final Token valueToken = value.getToken();
                final StringBuilder currentBody = getCurrentLevel().body;

                if (valueToken.type() == Token.Type.DATA)
                {
                    //Optimization
                    newLine(currentBody)
                            .append("var ")
                            .append(valueToken.value())
                            .append(' ')
                            .append(variable.getToken()
                                    .value());

                    break;
                }

                newLine(currentBody)
                        .append("var \"-1\" ")
                        .append(variable.getToken()
                                .value())
                        .append(convertNode(value, functions));

                newLine(currentBody);
                final String[] inputs = child.getInputs();

                if (inputs.length != 1) throw new AKeyException("autokey_akeylang.unknown_error", child.getToken());

                currentBody
                        .append("set ")
                        .append(inputs[0])
                        .append(' ')
                        .append(variable.getToken()
                                .value());
            }
            default -> getCurrentLevel().body.append(convertNode(root, functions));
        }

        return false;
    }

    private @NotNull StringBuilder convertNode(final @NotNull Node node, final HashMap<String, FunctionConverter> functions) throws AKeyException
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < node.getChildCount(); i++)
        {
            final Node n = node.getChild(i);
            final StringBuilder subSb = convertNode(n, functions);
            sb.append(subSb);
        }

        final Token token = node.getToken();
        switch (node.getType())
        {
            case TYPE ->
            {
                newLine(sb)
                        .append("var ")
                        .append(token.value())
                        .append(' ');
                final String name = getVariableName();
                sb.append(name);
                if (node.getParent() != null)
                    node.getParent()
                            .addInput(name);
            }
            case METHOD ->
            {
                final String functionName = token.value();

                if (functions.containsKey(functionName))
                {
                    final String labelNameVariable = getVariableName();
                    final String returnName = labelNumber.getLabelName();
                    newLine(sb)
                            .append("#Start of function call");
                    newLine(sb)
                            .append("var \"")
                            .append(returnName)
                            .append("\" variable$")
                            .append(functionName);
                    newLine(sb)
                            .append("var \"label$")
                            .append(functionName)
                            .append("\" ")
                            .append(labelNameVariable);
                    newLine(sb)
                            .append("jmp ")
                            .append(labelNameVariable);
                    newLine(sb)
                            .append(returnName)
                            .append(':');
                    newLine(sb)
                            .append("#End of function call");
                    break;
                }

                if (node.getParent() == null)
                {
                    newLine(sb).append(token.value());

                    for (String input : node.getInputs())
                        sb.append(' ')
                                .append(input);
                    break;
                }

                final String name = getVariableName();
                newLine(sb)
                        .append("var -1 ")
                        .append(name);

                newLine(sb).append(token.value());

                for (String input : node.getInputs())
                    sb.append(' ')
                            .append(input);

                if (Objects.equals(node.getToken()
                        .value(), "free")) throw new AKeyException("autokey_akeylang.free_error", node.getToken());

                node.getParent()
                        .addInput(name);
                sb.append(" >")
                        .append(name);
            }
            case OPERATOR ->
            {
                final String operatorName = switch (token.type())
                {
                    case UNDERSCORE -> "concat";
                    case PLUS -> "add";
                    case MINUS -> "subtract";
                    case ASTRIX -> "multiply";
                    case SLASH -> "divide";
                    case PERCENT -> "modulo";

                    case SMALLER_EQUALS, GREATER -> "greater";
                    case GREATER_EQUALS, SMALLER -> "smaller";

                    case AND -> "and";
                    case OR -> "or";
                    case BIT_AND -> "bitAnd";
                    case BIT_OR -> "bitOr";
                    case BIT_XOR -> "bitXor";

                    case EQUAL, NOT_EQUAL -> "equals";

                    default -> throw new AKeyException("autokey_akeylang.report_this", token);
                };

                final String name = getVariableName();
                newLine(sb)
                        .append("var -1 ")
                        .append(name);

                newLine(sb).append(operatorName);

                for (String input : node.getInputs())
                    sb.append(' ')
                            .append(input);

                if (node.getParent() == null) break;

                sb.append(" >")
                        .append(name);

                switch (token.type())
                {
                    case SMALLER_EQUALS, GREATER_EQUALS, NOT_EQUAL ->
                    {
                        final String tempName = getVariableName();

                        newLine(sb)
                                .append("var -1 ")
                                .append(tempName);

                        newLine(sb)
                                .append("invert ")
                                .append(name)
                                .append(" >")
                                .append(tempName);

                        node.getParent()
                                .addInput(tempName);
                    }
                    default -> node.getParent()
                            .addInput(name);
                }
            }
            case VARIABLE ->
            {
                if (node.getParent() != null)
                    node.getParent()
                            .addInput(token.value());
            }
            default -> throw AKeyException.unexpectedTokenException(token);
        }

        return sb;
    }

    private static final class Level
    {
        private final StringBuilder body;
        private final StringBuilder tail;

        public Level()
        {
            body = new StringBuilder();
            tail = new StringBuilder();
        }
    }
}