package com.cubicequation.autokey_akeylang;

import com.cubicequation.autokey_core.util.Numbers;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Locale;
import java.util.Optional;
import java.util.Stack;

public final class Evaluator
{
    public static final HashMap<Token.Type, Integer> OPERATOR_PRECEDENCES;

    static
    {
        OPERATOR_PRECEDENCES = new HashMap<>();

        OPERATOR_PRECEDENCES.put(Token.Type.OPENING_PARENTHESIS, 12);
        OPERATOR_PRECEDENCES.put(Token.Type.CLOSING_PARENTHESIS, 12);

        OPERATOR_PRECEDENCES.put(Token.Type.ASTRIX, 10);
        OPERATOR_PRECEDENCES.put(Token.Type.SLASH, 10);
        OPERATOR_PRECEDENCES.put(Token.Type.PERCENT, 10);

        OPERATOR_PRECEDENCES.put(Token.Type.UNDERSCORE, 9);
        OPERATOR_PRECEDENCES.put(Token.Type.PLUS, 9);
        OPERATOR_PRECEDENCES.put(Token.Type.MINUS, 9);

        OPERATOR_PRECEDENCES.put(Token.Type.SMALLER_EQUALS, 8);
        OPERATOR_PRECEDENCES.put(Token.Type.GREATER_EQUALS, 8);
        OPERATOR_PRECEDENCES.put(Token.Type.SMALLER, 8);
        OPERATOR_PRECEDENCES.put(Token.Type.GREATER, 8);

        OPERATOR_PRECEDENCES.put(Token.Type.EQUAL, 7);
        OPERATOR_PRECEDENCES.put(Token.Type.NOT_EQUAL, 7);

        OPERATOR_PRECEDENCES.put(Token.Type.BIT_AND, 6);

        OPERATOR_PRECEDENCES.put(Token.Type.BIT_XOR, 5);

        OPERATOR_PRECEDENCES.put(Token.Type.BIT_OR, 4);

        OPERATOR_PRECEDENCES.put(Token.Type.AND, 3);

        OPERATOR_PRECEDENCES.put(Token.Type.OR, 2);

        OPERATOR_PRECEDENCES.put(Token.Type.EQUALS, 0);
    }

    private Evaluator()
    {
        throw new IllegalStateException("Evaluator cannot be instantiated");
    }

    @Contract(" -> new")
    private static @NotNull AKeyException reportThisException()
    {
        final String typeName = "IGNORE TYPE";
        return new AKeyException("autokey_akeylang.report_this", new Token(Token.Type.DATA, typeName, -1, -1, typeName.length()));
    }

    public static @NotNull Node evaluateLine(final @NotNull Token @NotNull [] line, final int start) throws AKeyException
    {
        final Stack<Token> stack = new Stack<>();

        for (int i = line.length - 1; i >= start; i--)
            stack.push(line[i]);

        return evaluateLine(stack);
    }


    private static @NotNull Node evaluateLine(final @NotNull Stack<Token> stack) throws AKeyException
    {
        final Stack<Node> operands = new Stack<>();
        final Stack<Node> operators = new Stack<>();
        operators.push(new Node(new Token(Token.Type.OPENING_PARENTHESIS, "(", -1, -1, 1), Node.Type.METHOD));

        BREAK:
        while (!stack.isEmpty())
        {
            final Token token = stack.pop();

            switch (token.type())
            {
                case DATA -> operands.push(new Node(token, Node.Type.TYPE));
                case IDENTIFIER ->
                {
                    if (token.value()
                            .startsWith("variable$"))
                        throw new AKeyException("autokey_akeylang.illegal_variable_name", token);
                    if (token.value()
                            .startsWith("label$"))
                        throw new AKeyException("autokey_akeylang.illegal_variable_name", token);

                    if (!stack.isEmpty() && stack.peek()
                            .type() == Token.Type.OPENING_PARENTHESIS)
                    {
                        stack.push(token);
                        operands.push(evaluateMethod(stack));
                    }
                    else operands.push(new Node(token, Node.Type.VARIABLE));
                }
                case PLUS, MINUS, ASTRIX, SLASH, PERCENT, EQUALS, SMALLER_EQUALS, GREATER_EQUALS, SMALLER, GREATER, AND, OR, BIT_AND, BIT_OR, BIT_XOR, EQUAL, NOT_EQUAL, UNDERSCORE, OPENING_PARENTHESIS ->
                {
                    final int currentOperatorPrecedence = OPERATOR_PRECEDENCES.get(token.type());
                    while (operators.size() > 1)
                    {
                        final Token.Type previousOperatorType = operators.peek()
                                .getToken()
                                .type();
                        if (previousOperatorType == Token.Type.OPENING_PARENTHESIS) break;

                        final int previousOperatorPrecedence = OPERATOR_PRECEDENCES.get(previousOperatorType);
                        if (previousOperatorPrecedence < currentOperatorPrecedence) break;

                        evaluateExpression(operands, operators);
                    }

                    operators.push(new Node(token, token.type() == Token.Type.EQUALS ? Node.Type.ASSIGN : Node.Type.OPERATOR));
                }
                case CLOSING_PARENTHESIS ->
                {
                    boolean isOpeningParenthesis = operators.peek()
                            .getToken()
                            .type() == Token.Type.OPENING_PARENTHESIS;
                    while (operators.size() > 1 && !isOpeningParenthesis)
                    {
                        evaluateExpression(operands, operators);
                        isOpeningParenthesis = operators.peek()
                                .getToken()
                                .type() == Token.Type.OPENING_PARENTHESIS;
                    }

                    if (isOpeningParenthesis) operators.pop();
                    if (operators.isEmpty())
                    {
                        stack.push(token);
                        break BREAK;
                    }
                }
                case ARGUMENT_SEPARATOR ->
                {
                    break BREAK;
                }
                default -> throw AKeyException.unexpectedTokenException(token);
            }
        }

        while (!operators.isEmpty() && operators.peek()
                .getToken()
                .type() != Token.Type.OPENING_PARENTHESIS)
            evaluateExpression(operands, operators);

        if (operands.isEmpty()) throw reportThisException();
        return operands.pop();
    }

    private static void evaluateExpression(final @NotNull Stack<Node> operands, final @NotNull Stack<Node> operators) throws AKeyException
    {
        if (operators.isEmpty()) throw reportThisException();

        final Node operator;
        final Node rightOperand;
        final Node leftOperand;

        if (operands.size() < 2)
        {
            final Token.Type tokenType = operators.peek()
                    .getToken()
                    .type();
            if (tokenType != Token.Type.PLUS && tokenType != Token.Type.MINUS)
                throw reportThisException();

            operator = operators.pop();
            rightOperand = operands.pop();
            leftOperand = new Node(new Token(Token.Type.DATA, "0", rightOperand.getToken()
                    .line(), rightOperand.getToken()
                    .start(), 1), Node.Type.TYPE);
        }
        else
        {
            operator = operators.pop();
            rightOperand = operands.pop();
            leftOperand = operands.pop();
        }

        //Optimization
        if (rightOperand.getType() == Node.Type.TYPE && leftOperand.getType() == Node.Type.TYPE)
        {
            final Token leftToken = leftOperand.getToken();
            final Token rightToken = rightOperand.getToken();
            final Token operatorToken = operator.getToken();

            final Object result = switch (operatorToken.type())
            {
                case UNDERSCORE ->
                {
                    final String leftValue = leftToken.value();
                    final String rightValue = rightToken.value();
                    yield '"' + leftValue.substring(1, leftValue.length() - 1) + rightValue.substring(1, rightValue.length() - 1) + '"';
                }
                case PLUS -> parseNumber(leftToken) + parseNumber(rightToken);
                case MINUS -> parseNumber(leftToken) - parseNumber(rightToken);
                case ASTRIX -> parseNumber(leftToken) * parseNumber(rightToken);
                case SLASH -> parseNumber(leftToken) / parseNumber(rightToken);
                case PERCENT -> parseNumber(leftToken) % parseNumber(rightToken);

                case SMALLER_EQUALS -> parseNumber(leftToken) <= parseNumber(rightToken);
                case GREATER_EQUALS -> parseNumber(leftToken) >= parseNumber(rightToken);
                case SMALLER -> parseNumber(leftToken) < parseNumber(rightToken);
                case GREATER -> parseNumber(leftToken) > parseNumber(rightToken);

                case AND -> parseBoolean(leftToken) && parseBoolean(rightToken);
                case OR -> parseBoolean(leftToken) || parseBoolean(rightToken);
                case BIT_AND -> parseBoolean(leftToken) & parseBoolean(rightToken);
                case BIT_OR -> parseBoolean(leftToken) | parseBoolean(rightToken);
                case BIT_XOR -> parseBoolean(leftToken) ^ parseBoolean(rightToken);

                case EQUAL ->
                {
                    final String string1 = leftToken.value();
                    final String string2 = rightToken.value();

                    try
                    {
                        yield parseNumber(leftToken) == parseNumber(rightToken);
                    } catch (AKeyException ignored)
                    {
                    }

                    yield string1.equals(string2);
                }
                case NOT_EQUAL ->
                {
                    final String string1 = leftToken.value();
                    final String string2 = rightToken.value();

                    try
                    {
                        yield parseNumber(leftToken) != parseNumber(rightToken);
                    } catch (AKeyException ignored)
                    {
                    }

                    yield (!string1.equals(string2));
                }

                default -> throw new AKeyException("autokey_akeylang.report_this", operatorToken);
            };

            final String stringResult = result instanceof String ? result.toString() : result.toString()
                    .toLowerCase(Locale.ROOT);
            operands.push(new Node(new Token(Token.Type.DATA, stringResult, operatorToken.line(), operatorToken.start(), stringResult.length()), Node.Type.TYPE));
            return;
        }


        operator.addChild(leftOperand);
        operator.addChild(rightOperand);

        operands.push(operator);
    }

    private static @NotNull Node evaluateMethod(final @NotNull Stack<Token> stack) throws AKeyException
    {
        final Node root = new Node(stack.pop(), Node.Type.METHOD);
        Token token = stack.pop();

        while (!stack.isEmpty())
        {
            token = stack.peek();

            if (token.type() == Token.Type.CLOSING_PARENTHESIS)
            {
                stack.pop();
                return root;
            }

            root.addChild(evaluateLine(stack));
        }

        throw AKeyException.missingTokenException(token);
    }

    private static double parseNumber(final @NotNull Token token) throws AKeyException
    {
        final String string = token.value();

        Optional<Integer> i = Numbers.parseInt(string);
        if (i.isPresent()) return i.get();

        Optional<Double> d = Numbers.parseDouble(string);
        if (d.isPresent()) return d.get();

        Optional<Float> f = Numbers.parseFloat(string);
        if (f.isPresent()) return f.get();

        throw new AKeyException("autokey_akeylang.not_a_number", token, string);
    }

    private static boolean parseBoolean(final @NotNull Token token) throws AKeyException
    {
        final String string = token.value();

        if (string.equals("true")) return true;
        if (string.equals("false")) return false;

        throw new AKeyException("autokey_akeylang.not_a_boolean", token, string);
    }
}
