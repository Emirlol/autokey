package com.cubicequation.autokey_akeylang;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;

public final class Node
{
    private final ArrayList<Node> children;
    private final ArrayList<String> inputs;
    private final Token token;
    private final Type type;
    private Node parent;

    public Node(final @NotNull Token token, final @NotNull Node.Type type)
    {
        this.token = token;
        this.type = type;
        this.parent = null;
        children = new ArrayList<>();
        inputs = new ArrayList<>();
    }

    public @Nullable Node getParent()
    {
        return parent;
    }

    public void addChild(final @NotNull Node child)
    {
        child.parent = this;
        children.add(child);
    }

    public @NotNull Node getChild(final int index)
    {
        return children.get(index);
    }

    public int getChildCount()
    {
        return children.size();
    }

    public @NotNull Node.Type getType()
    {
        return type;
    }

    public @NotNull Token getToken()
    {
        return token;
    }

    public void addInput(final @NotNull String name)
    {
        inputs.add(name);
    }

    public @NotNull String[] getInputs()
    {
        return inputs.toArray(String[]::new);
    }

    public enum Type
    {
        TYPE,
        METHOD,
        VAR,
        OPERATOR,
        VARIABLE,
        ASSIGN,
        FUNCTION,
        RETURN,
        IF,
        WHILE,
        CLOSE
    }
}
