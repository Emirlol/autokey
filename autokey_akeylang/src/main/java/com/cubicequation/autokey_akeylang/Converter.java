package com.cubicequation.autokey_akeylang;

import com.cubicequation.autokey_core.language.Library;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

public final class Converter
{
    private Converter()
    {
        throw new IllegalStateException("Converter cannot be instantiated");
    }

    @Contract("_ -> param1")
    private static @NotNull StringBuilder newLine(final @NotNull StringBuilder sb)
    {
        return sb.append(System.getProperty("line.separator"));
    }

    public static @NotNull CharSequence convert(final @NotNull Node @NotNull [] roots) throws AKeyException
    {
        final LabelNumber labelNumber = new LabelNumber();
        FunctionConverter currentFunction = new FunctionConverter("null", labelNumber);

        final HashMap<String, FunctionConverter> functions = new HashMap<>();
        functions.put(null, currentFunction);

        for (Node root : roots)
        {
            final Token token = root.getToken();
            if (root.getType() != Node.Type.FUNCTION || Library.containsFunction(token.value())) continue;

            final String name = token.value();
            functions.put(name, new FunctionConverter(name, labelNumber));
        }

        for (Node root : roots)
        {
            if (root.getType() == Node.Type.FUNCTION)
            {
                final FunctionConverter newFunction = functions.get(root.getToken()
                        .value());

                if (newFunction == null) throw new AKeyException("autokey_akeylang.report_this", root.getToken());

                currentFunction = newFunction;

                continue;
            }

            if (currentFunction.appendTree(root, functions))
                currentFunction = functions.get(null);
        }

        final StringBuilder sb = new StringBuilder();

        newLine(sb)
                .append("\"label$end\" variable$null");
        newLine(sb)
                .append("1 variable$1");

        functions.forEach((key, value) ->
        {
            newLine(sb);
            newLine(sb);
            newLine(sb)
                    .append("#Start of function definition");

            newLine(sb)
                    .append("label$")
                    .append(key)
                    .append(':');

            newLine(sb)
                    .append("newStack");

            newLine(sb)
                    .append(value.get());

            newLine(sb)
                    .append("stackAmount variable$oldStackSize-")
                    .append(key);
            newLine(sb)
                    .append("subtract variable$oldStackSize-")
                    .append(key)
                    .append(" variable$1 >variable$newStackSize-")
                    .append(key);
            newLine(sb)
                    .append("stack variable$newStackSize-")
                    .append(key);

            newLine(sb)
                    .append("jmp variable$")
                    .append(key);

            newLine(sb)
                    .append("#End of function definition");
        });

        newLine(sb);
        newLine(sb);
        newLine(sb)
                .append("label$end:");

        return sb.delete(0, 1);
    }

    public static class LabelNumber
    {
        int labelNumber;

        LabelNumber()
        {
            labelNumber = 999;
        }

        @NotNull String getLabelName()
        {
            labelNumber++;
            return "label$" + labelNumber;
        }
    }
}
