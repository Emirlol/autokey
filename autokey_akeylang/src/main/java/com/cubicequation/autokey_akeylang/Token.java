package com.cubicequation.autokey_akeylang;

import org.jetbrains.annotations.NotNull;

public record Token(@NotNull Token.Type type, @NotNull String value, int line, int start, int length)
{
    public enum Type
    {
        IDENTIFIER,
        DATA,
        EQUALS,
        VAR,

        FUNCTION,
        RETURN,

        IF,
        WHILE,

        CLOSE,

        OPENING_PARENTHESIS,
        CLOSING_PARENTHESIS,

        UNDERSCORE,
        PLUS,
        MINUS,
        ASTRIX,
        SLASH,
        PERCENT,

        SMALLER_EQUALS,
        GREATER_EQUALS,
        SMALLER,
        GREATER,

        AND,
        OR,
        BIT_AND,
        BIT_OR,
        BIT_XOR,

        EQUAL,
        NOT_EQUAL,
        ARGUMENT_SEPARATOR
    }
}
