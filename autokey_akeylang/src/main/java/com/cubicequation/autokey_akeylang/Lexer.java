package com.cubicequation.autokey_akeylang;

import com.cubicequation.autokey_core.language.Data;
import com.cubicequation.autokey_core.util.StringUtil;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public final class Lexer
{
    private static final Character[] IGNORED_CHARS = new Character[]{'{', ';'};
    private static final Character[] SPECIAL_CHARS = new Character[]{'(', ')', '+', '-', '*', '/', '%', ','};

    private Lexer()
    {
        throw new IllegalStateException("Lexer cannot be instantiated");
    }

    @Contract("_ -> new")
    public static @NotNull Token @NotNull [] @NotNull [] getTokens(final @NotNull Scanner scanner)
    {
        final ArrayList<ArrayList<Token>> tokens = new ArrayList<>();

        int lineCount = 0;
        ArrayList<Token> currentLine = new ArrayList<>();
        StringBuilder lexeme = new StringBuilder();
        boolean isString = false;

        while (scanner.hasNextLine())
        {
            lineCount++;
            final StringBuilder line = new StringBuilder(scanner.nextLine());

            if (isString) lexeme.append('\n');
            else if (line.isEmpty()) continue;

            final Integer[] spacePositions = StringUtil.getSpacePositions(line);

            for (int position = 0; position < line.length(); position++)
            {
                final char character = line.charAt(position);
                final boolean isSpecialChar = Arrays.asList(SPECIAL_CHARS)
                        .contains(character);

                if ((Character.isWhitespace(character) || isSpecialChar) && !isString)
                {
                    if (!lexeme.isEmpty())
                    {
                        final String subLine = line.substring(0, position)
                                .replace(" ", "");
                        final StringBuilder subOriginalBuilder = new StringBuilder(subLine);

                        for (int pos : spacePositions)
                        {
                            if (pos >= subOriginalBuilder.length()) break;
                            subOriginalBuilder.insert(pos, ' ');
                        }

                        currentLine.add(getToken(lexeme.toString(), lineCount, subOriginalBuilder.length()));
                        lexeme = new StringBuilder();
                    }

                    if (isSpecialChar) line.insert(position + 1, ' ');
                    else continue;
                }
                else if (Arrays.asList(IGNORED_CHARS)
                        .contains(character) && !isString)
                {
                    continue;
                }
                else if (character == '#' && !isString) break;
                else if (character == '"')
                {
                    isString = !isString;

                    if (isString && !lexeme.isEmpty())
                    {
                        final String subLine = line.substring(0, position)
                                .replace(" ", "");
                        final StringBuilder subOriginalBuilder = new StringBuilder(subLine);

                        for (int pos : spacePositions)
                        {
                            if (pos >= subOriginalBuilder.length()) break;
                            subOriginalBuilder.insert(pos, ' ');
                        }

                        currentLine.add(getToken(lexeme.toString(), lineCount, subOriginalBuilder.length()));
                        lexeme = new StringBuilder();
                    }
                    else if (!isString) line.insert(position + 1, ' ');
                }

                lexeme.append(character);
            }

            if (!lexeme.isEmpty() && !isString)
            {
                currentLine.add(getToken(lexeme.toString(), lineCount, line.length()));
                lexeme = new StringBuilder();
            }

            if (!currentLine.isEmpty() && !isString)
            {
                tokens.add(currentLine);
                currentLine = new ArrayList<>();
            }
        }

        scanner.close();

        if (!currentLine.isEmpty()) tokens.add(currentLine);

        return tokens.stream()
                .map(u -> u.toArray(Token[]::new))
                .toArray(Token[][]::new);
    }

    @Contract("_, _, _ -> new")
    private static @NotNull Token getToken(final @NotNull String lexeme, final int line, final int position)
    {
        final int pos = position - lexeme.length();

        Token.Type tokenType = switch (lexeme)
        {
            case "function" -> Token.Type.FUNCTION;
            case "return" -> Token.Type.RETURN;
            case "var" -> Token.Type.VAR;

            case "if" -> Token.Type.IF;
            case "while" -> Token.Type.WHILE;

            case "}" -> Token.Type.CLOSE;
            case "," -> Token.Type.ARGUMENT_SEPARATOR;

            case "(" -> Token.Type.OPENING_PARENTHESIS;
            case ")" -> Token.Type.CLOSING_PARENTHESIS;

            case "_" -> Token.Type.UNDERSCORE;
            case "+" -> Token.Type.PLUS;
            case "-" -> Token.Type.MINUS;
            case "/" -> Token.Type.SLASH;
            case "*" -> Token.Type.ASTRIX;
            case "%" -> Token.Type.PERCENT;

            case "&&" -> Token.Type.AND;
            case "||" -> Token.Type.OR;
            case "&" -> Token.Type.BIT_AND;
            case "|" -> Token.Type.BIT_OR;
            case "^" -> Token.Type.BIT_XOR;

            case "<=" -> Token.Type.SMALLER_EQUALS;
            case ">=" -> Token.Type.GREATER_EQUALS;
            case "<" -> Token.Type.SMALLER;
            case ">" -> Token.Type.GREATER;

            case "==" -> Token.Type.EQUAL;
            case "!=" -> Token.Type.NOT_EQUAL;

            case "=" -> Token.Type.EQUALS;

            default -> Token.Type.IDENTIFIER;
        };

        if (Data.isData(lexeme))
        {
            tokenType = Token.Type.DATA;
            return new Token(tokenType, lexeme, line, pos, lexeme.length());
        }

        return new Token(tokenType, lexeme, line, pos, lexeme.length());
    }
}
