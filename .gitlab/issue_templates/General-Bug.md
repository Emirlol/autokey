## Summary

(Summarize the bug encountered concisely)

## Steps to reproduce

(How one can reproduce the issue - this is very important)

## What is the current bug behavior?

(What actually happens)

## What is the expected correct behavior?

(What you should see instead)

## Relevant logs and/or screenshots

(Paste any relevant logs - please use code blocks (```) to format them as it's very hard to read otherwise.  
You can find the Minecraft logs in the .minecraft/logs folder)

## Possible fixes

(If you can, link to the line of code that might be responsible for the problem)
