package com.cubicequation.autokey_standardlibrary_vanillakeybindings.entrypoints;

import com.cubicequation.autokey_standardlibrary.api.KeyBindingsAPI;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.option.GameOptions;
import net.minecraft.client.option.KeyBinding;

import java.util.HashMap;
import java.util.Map;

public class VanillaKeyBindings implements KeyBindingsAPI
{
    @Override
    public Map<String, ? extends KeyBinding> getKeyBindings()
    {
        final HashMap<String, KeyBinding> keyBindings = new HashMap<>();

        final GameOptions options = MinecraftClient.getInstance().options;

        if (options == null) return new HashMap<>();

        keyBindings.put("forward", options.forwardKey);
        keyBindings.put("left", options.leftKey);
        keyBindings.put("back", options.backKey);
        keyBindings.put("right", options.rightKey);
        keyBindings.put("jump", options.jumpKey);
        keyBindings.put("sneak", options.sneakKey);
        keyBindings.put("sprint", options.sprintKey);

        keyBindings.put("inventory", options.inventoryKey);
        keyBindings.put("swapHands", options.swapHandsKey);
        keyBindings.put("drop", options.dropKey);
        keyBindings.put("use", options.useKey);
        keyBindings.put("attack", options.attackKey);
        keyBindings.put("pickItem", options.pickItemKey);

        keyBindings.put("chat", options.chatKey);
        keyBindings.put("playerList", options.playerListKey);
        keyBindings.put("command", options.commandKey);
        keyBindings.put("socialInteractions", options.socialInteractionsKey);
        keyBindings.put("screenshot", options.screenshotKey);
        keyBindings.put("togglePerspective", options.togglePerspectiveKey);
        keyBindings.put("smoothCamera", options.smoothCameraKey);
        keyBindings.put("fullscreen", options.fullscreenKey);
        keyBindings.put("spectatorOutlines", options.spectatorOutlinesKey);
        keyBindings.put("advancements", options.advancementsKey);

        keyBindings.put("saveToolbarActivator", options.saveToolbarActivatorKey);
        keyBindings.put("loadToolbarActivator", options.loadToolbarActivatorKey);

        return keyBindings;
    }
}
