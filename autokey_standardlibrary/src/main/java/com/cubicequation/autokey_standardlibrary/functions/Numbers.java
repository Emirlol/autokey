package com.cubicequation.autokey_standardlibrary.functions;

import com.cubicequation.autokey_core.language.functions.LibraryBuilder;
import org.jetbrains.annotations.NotNull;

public final class Numbers
{
    private Numbers()
    {
        throw new IllegalStateException("Numbers cannot be instantiated");
    }

    public static void initialize(@NotNull LibraryBuilder builder)
    {
        //TODO change when try, catch is implemented
        builder.addROverload("parseFloat",
                arguments -> com.cubicequation.autokey_core.util.Numbers.parseFloat(arguments[0].toString())
                        .orElse(Float.NaN),
                Float.class, Object.class);

        builder.addROverload("parseInt",
                arguments -> Integer.parseInt(arguments[0].toString()),
                Integer.class, Object.class);

        builder.addROverload("parseLong",
                arguments -> Long.parseLong(arguments[0].toString()),
                Long.class, Object.class);

        builder.addROverload("parseDouble",
                arguments -> Double.parseDouble(arguments[0].toString()),
                Double.class, Object.class);

        builder.addROverload("parseByte",
                arguments -> Byte.parseByte(arguments[0].toString()),
                Byte.class, Object.class);

        builder.addROverload("parseShort",
                arguments -> Short.parseShort(arguments[0].toString()),
                Short.class, Object.class);

        builder.addROverload("random",
                arguments -> Math.random() * (((Number) arguments[1]).doubleValue() - ((Number) arguments[0]).doubleValue()) + ((Number) arguments[0]).doubleValue(),
                Double.class, Number.class, Number.class);

        builder.addROverload("round",
                arguments -> Math.round(((Number) arguments[0]).doubleValue()),
                Long.class, Number.class);

        builder.addROverload("add",
                arguments -> ((Number) arguments[0]).doubleValue() + ((Number) arguments[1]).doubleValue(),
                Double.class, Number.class, Number.class);

        builder.addROverload("subtract",
                arguments -> ((Number) arguments[0]).doubleValue() - ((Number) arguments[1]).doubleValue(),
                Double.class, Number.class, Number.class);

        builder.addROverload("multiply",
                arguments -> ((Number) arguments[0]).doubleValue() * ((Number) arguments[1]).doubleValue(),
                Double.class, Number.class, Number.class);

        builder.addROverload("divide",
                arguments -> ((Number) arguments[0]).doubleValue() / ((Number) arguments[1]).doubleValue(),
                Double.class, Number.class, Number.class);

        builder.addROverload("power",
                arguments -> Math.pow(((Number) arguments[0]).doubleValue(), ((Number) arguments[1]).doubleValue()),
                Double.class, Number.class, Number.class);

        builder.addROverload("modulo",
                arguments -> ((Number) arguments[0]).doubleValue() % ((Number) arguments[1]).doubleValue(),
                Double.class, Number.class, Number.class);

        builder.addROverload("smaller",
                arguments -> ((Number) arguments[0]).doubleValue() < ((Number) arguments[1]).doubleValue(),
                Boolean.class, Number.class, Number.class);

        builder.addROverload("greater",
                arguments -> ((Number) arguments[0]).doubleValue() > ((Number) arguments[1]).doubleValue(),
                Boolean.class, Number.class, Number.class);
    }
}
