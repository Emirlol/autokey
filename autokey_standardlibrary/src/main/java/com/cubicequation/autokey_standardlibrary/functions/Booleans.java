package com.cubicequation.autokey_standardlibrary.functions;

import com.cubicequation.autokey_core.language.functions.LibraryBuilder;
import org.jetbrains.annotations.NotNull;

public final class Booleans
{
    private Booleans()
    {
        throw new IllegalStateException("Booleans cannot be instantiated");
    }

    public static void initialize(@NotNull LibraryBuilder builder)
    {
        builder.addROverload("parseBool",
                arguments ->
                {
                    final String string = arguments[0].toString();
                    if (string.equals("true")) return true;
                    if (string.equals("false")) return false;

                    throw new NumberFormatException("For input string: \"" + string + '"');
                },
                Boolean.class, Object.class);

        builder.addROverload("invert",
                arguments -> !(boolean) arguments[0],
                Boolean.class, Boolean.class);

        builder.addROverload("and",
                arguments -> (boolean) arguments[0] && (boolean) arguments[1],
                Boolean.class, Boolean.class, Boolean.class);

        builder.addROverload("or",
                arguments -> (boolean) arguments[0] || (boolean) arguments[1],
                Boolean.class, Boolean.class, Boolean.class);

        builder.addROverload("equals",
                arguments ->
                {
                    final Object object1 = arguments[0];
                    final Object object2 = arguments[1];

                    if (object1 instanceof Number number1 && object2 instanceof Number number2)
                        return number1.doubleValue() == number2.doubleValue();

                    return object1.equals(object2) || object2.equals(object1);
                },
                Boolean.class, Object.class, Object.class);
    }
}
