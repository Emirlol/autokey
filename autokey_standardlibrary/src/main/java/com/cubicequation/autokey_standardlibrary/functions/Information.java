package com.cubicequation.autokey_standardlibrary.functions;

import com.cubicequation.autokey_core.language.exceptions.RuntimeException;
import com.cubicequation.autokey_core.language.functions.LibraryBuilder;
import com.cubicequation.autokey_standardlibrary.entrypoints.Keybindings;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.option.KeyBinding;
import org.jetbrains.annotations.NotNull;

public final class Information
{
    private Information()
    {
        throw new IllegalStateException("Information cannot be instantiated");
    }

    public static void initialize(@NotNull LibraryBuilder builder)
    {
        builder.addROverload("getKey",
                arguments ->
                {
                    final String keyName = (String) arguments[0];

                    final KeyBinding key = Keybindings.getKeyBinding(keyName);
                    if (key == null) throw new RuntimeException("autokey_standardlibrary.information.unknown_keybinding", keyName);

                    return key.isPressed();
                },
                Boolean.class, String.class);

        builder.addROverload("getPositionX",
                arguments -> MinecraftClient.getInstance().player != null ? MinecraftClient.getInstance().player.getX() : Double.NaN,
                Double.class);

        builder.addROverload("getPositionY",
                arguments -> MinecraftClient.getInstance().player != null ? MinecraftClient.getInstance().player.getY() : Double.NaN,
                Double.class);

        builder.addROverload("getPositionZ",
                arguments -> MinecraftClient.getInstance().player != null ? MinecraftClient.getInstance().player.getZ() : Double.NaN,
                Double.class);
    }
}
