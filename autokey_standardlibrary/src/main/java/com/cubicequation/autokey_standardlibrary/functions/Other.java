package com.cubicequation.autokey_standardlibrary.functions;

import com.cubicequation.autokey_core.language.exceptions.RuntimeException;
import com.cubicequation.autokey_core.language.functions.LibraryBuilder;
import com.cubicequation.autokey_core.util.Feedback;
import com.mojang.logging.LogUtils;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayNetworkHandler;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Range;

public final class Other
{
    private Other()
    {
        throw new IllegalStateException("Other cannot be instantiated");
    }

    public static void initialize(@NotNull LibraryBuilder builder)
    {
        builder.addROverload("ping",
                arguments ->
                {
                    Feedback.sendClientMessage("Pong!");
                    return null;
                },
                null);

        builder.addNROverload("print",
                arguments ->
                {
                    final String content = arguments[0].toString();
                    Feedback.sendClientMessage(content);
                }, Object.class);

        builder.addNROverload("printPublic",
                arguments ->
                {
                    final String content = arguments[0].toString();

                    final ClientPlayNetworkHandler networkHandler =
                            MinecraftClient.getInstance()
                                    .getNetworkHandler();

                    if (networkHandler == null) throw new RuntimeException("autokey_standardlibrary.other.network_handler_null");

                    networkHandler.sendChatMessage(content);
                }, Object.class);

        builder.addNROverload("printCommand",
                arguments ->
                {
                    final String content = arguments[0].toString();

                    final ClientPlayNetworkHandler networkHandler =
                            MinecraftClient.getInstance()
                                    .getNetworkHandler();

                    if (networkHandler == null) throw new RuntimeException("autokey_standardlibrary.other.network_handler_null");

                    networkHandler.sendChatCommand(content);
                }, Object.class);

        builder.addNROverload("wait",
                arguments ->
                {
                    @Range(from = 0, to = Long.MAX_VALUE) final long delay = (long) arguments[0];
                    if (delay < 0) throw new RuntimeException("autokey_standardlibrary.other.negative_time_amount");

                    try
                    {
                        Thread.sleep(delay);
                    } catch (InterruptedException e)
                    {
                        LogUtils.getLogger()
                                .error("An InterruptionException was thrown while calling 'wait'.", e);
                    }
                }, Long.class);

        builder.addNROverload("throw",
                arguments ->
                {
                    throw new RuntimeException(arguments[0].toString());
                }, String.class);
    }
}
