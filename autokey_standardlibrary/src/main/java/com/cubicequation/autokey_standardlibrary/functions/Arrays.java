package com.cubicequation.autokey_standardlibrary.functions;

import com.cubicequation.autokey_core.language.functions.LibraryBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Range;

public final class Arrays
{
    private Arrays()
    {
        throw new IllegalStateException("Other cannot be instantiated");
    }

    public static void initialize(@NotNull LibraryBuilder builder)
    {
        builder.addROverload("getArrayLength",
                arguments ->
                {
                    final Object[] objects = (Object[]) arguments[0];
                    return objects.length;
                },
                Integer.class, Object[].class);

        builder.addROverload("getArrayElementAt",
                arguments ->
                {
                    @Range(from = 0, to = Integer.MAX_VALUE - 8) final int position = (int) arguments[1];
                    final Object[] objects = (Object[]) arguments[0];
                    return objects[position];
                },
                Object.class, Object[].class, Integer.class);
    }
}
