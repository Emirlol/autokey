package com.cubicequation.autokey_standardlibrary.functions;

import com.cubicequation.autokey_core.language.exceptions.RuntimeException;
import com.cubicequation.autokey_core.language.functions.LibraryBuilder;
import org.jetbrains.annotations.NotNull;

public final class Strings
{
    private Strings()
    {
        throw new IllegalStateException("Strings cannot be instantiated");
    }

    public static void initialize(@NotNull LibraryBuilder builder)
    {
        builder.addROverload("toString",
                arguments -> arguments[0].toString(),
                String.class, Object.class);

        builder.addROverload("concat",
                arguments -> arguments[0] + (String) arguments[1],
                String.class, String.class, String.class);

        builder.addROverload("contains",
                arguments -> arguments[0].toString()
                        .contains(arguments[1].toString()),
                Boolean.class, String.class, String.class);

        builder.addROverload("startsWith",
                arguments -> arguments[0].toString()
                        .startsWith(arguments[1].toString()),
                Boolean.class, String.class, String.class);

        builder.addROverload("endsWith",
                arguments -> arguments[0].toString()
                        .endsWith(arguments[1].toString()),
                Boolean.class, String.class, String.class);

        builder.addROverload("trim",
                arguments -> arguments[0].toString()
                        .trim(),
                String.class, String.class);

        builder.addROverload("skipName",
                arguments -> arguments[0].toString()
                        .replaceFirst("<[^>]+> ", ""),
                String.class, String.class);

        builder.addROverload("replace",
                arguments -> arguments[0].toString()
                        .replace(arguments[1].toString(), arguments[2].toString()),
                String.class, String.class, String.class, String.class);

        builder.addROverload("replaceRegex",
                arguments -> arguments[0].toString()
                        .replaceAll(arguments[1].toString(), arguments[2].toString()),
                String.class, String.class, String.class, String.class);

        builder.addROverload("replaceFirstRegex",
                arguments -> arguments[0].toString()
                        .replaceFirst(arguments[1].toString(), arguments[2].toString()),
                String.class, String.class, String.class, String.class);

        builder.addROverload("stringLength",
                arguments -> ((CharSequence) arguments[0]).length(),
                Integer.class, CharSequence.class);

        builder.addROverload("split",
                arguments -> arguments[0].toString()
                        .split(arguments[1].toString()),
                String[].class, String.class, String.class);

        builder.addROverload("split",
                arguments -> arguments[0].toString()
                        .split(arguments[1].toString(), (int) arguments[2]),
                String[].class, String.class, String.class, Integer.class);

        builder.addROverload("subString",
                arguments ->
                {
                    final String string = arguments[0].toString();
                    final int start = (int) arguments[1];
                    final int end = (int) arguments[2];

                    if (start < 0)
                        throw new RuntimeException("autokey_standardlibrary.strings.too_early_sub_string_start");
                    if (end > string.length())
                        throw new RuntimeException("autokey_standardlibrary.strings.too_late_sub_string_end");
                    if (start > end)
                        throw new RuntimeException("autokey_standardlibrary.strings.too_early_sub_string_end");

                    return string.substring(start, end);
                },
                String.class, String.class, Integer.class, Integer.class);
    }
}
