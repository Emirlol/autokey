package com.cubicequation.autokey_standardlibrary.functions;

import com.cubicequation.autokey_core.language.exceptions.RuntimeException;
import com.cubicequation.autokey_core.language.functions.LibraryBuilder;
import com.cubicequation.autokey_standardlibrary.AimLock;
import com.cubicequation.autokey_standardlibrary.entrypoints.Keybindings;
import com.cubicequation.autokey_standardlibrary.mixins.KeyBindingAccessor;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.client.option.KeyBinding;
import net.minecraft.entity.player.PlayerInventory;
import org.jetbrains.annotations.NotNull;

public final class Interactions
{
    private static final String MINECRAFT_PLAYER_NULL_MESSAGE = "autokey_standardlibrary.interactions.player_null";

    private Interactions()
    {
        throw new IllegalStateException("Interactions cannot be instantiated");
    }

    public static void initialize(@NotNull LibraryBuilder builder)
    {
        builder.addNROverload("setKey",
                arguments ->
                {
                    final String keyName = (String) arguments[0];
                    final boolean value = (Boolean) arguments[1];

                    final KeyBinding key = Keybindings.getKeyBinding(keyName);
                    if (key == null) throw new RuntimeException("autokey_standardlibrary.interactions.unknown_keybinding", keyName);

                    key.setPressed(value);
                }, String.class, Boolean.class);

        builder.addNROverload("pressKey",
                arguments ->
                {
                    final String keyName = (String) arguments[0];

                    final KeyBinding key = Keybindings.getKeyBinding(keyName);
                    if (key == null) throw new RuntimeException("autokey_standardlibrary.interactions.unknown_keybinding", keyName);

                    final KeyBindingAccessor accessor = (KeyBindingAccessor) key;
                    accessor.setTimesPressed(accessor.getTimesPressed() + 1);
                }, String.class);

        builder.addNROverload("slot",
                arguments ->
                {
                    final int slot = (Integer) arguments[0];

                    if (!PlayerInventory.isValidHotbarIndex(slot))
                        throw new RuntimeException("autokey_standardlibrary.interactions.slot_out_of_range", slot);

                    ClientPlayerEntity player = MinecraftClient.getInstance().player;
                    if (player == null)
                        throw new RuntimeException(MINECRAFT_PLAYER_NULL_MESSAGE);

                    player.getInventory().selectedSlot = slot;
                }, Integer.class);

        builder.addNROverload("setYaw",
                arguments ->
                {
                    ClientPlayerEntity player = MinecraftClient.getInstance().player;

                    if (player == null)
                        throw new RuntimeException(MINECRAFT_PLAYER_NULL_MESSAGE);

                    player.setYaw((Float) arguments[0]);
                }, Float.class);

        builder.addNROverload("setPitch",
                arguments ->
                {
                    ClientPlayerEntity player = MinecraftClient.getInstance().player;

                    if (player == null)
                        throw new RuntimeException(MINECRAFT_PLAYER_NULL_MESSAGE);

                    player.setPitch((Float) arguments[0]);
                }, Float.class);

        builder.addNROverload("setAimlock",
                arguments -> AimLock.setLocked((Boolean) arguments[0]),
                Boolean.class);

        builder.addNROverload("setMaxFPS",
                arguments ->
                {
                    final int value = (Integer) arguments[0];

                    if (value < 10 || value > 260)
                        throw new RuntimeException("autokey_standardlibrary.interactions.wrong_fps_amount");

                    final MinecraftClient client = MinecraftClient.getInstance();

                    client.options.getMaxFps()
                            .setValue(value);
                }, Integer.class);

        builder.addNROverload("jump", arguments ->
        {
            ClientPlayerEntity player = MinecraftClient.getInstance().player;

            if (player == null)
                throw new RuntimeException(MINECRAFT_PLAYER_NULL_MESSAGE);

            player.jump();
        });
    }
}
