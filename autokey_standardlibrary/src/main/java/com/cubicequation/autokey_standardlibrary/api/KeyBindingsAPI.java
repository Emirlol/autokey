package com.cubicequation.autokey_standardlibrary.api;

import net.minecraft.client.option.KeyBinding;

import java.util.Map;

public interface KeyBindingsAPI
{
    Map<String, ? extends KeyBinding> getKeyBindings();
}
