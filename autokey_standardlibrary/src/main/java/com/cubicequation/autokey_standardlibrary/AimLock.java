package com.cubicequation.autokey_standardlibrary;

public final class AimLock
{
    private static boolean locked;

    private AimLock()
    {
        throw new IllegalStateException("AimLock cannot be instantiated");
    }

    public static boolean isLocked()
    {
        return locked;
    }

    public static void setLocked(boolean value)
    {
        if (value == locked) return;

        locked = value;
    }
}
