package com.cubicequation.autokey_standardlibrary.mixins;

import com.cubicequation.autokey_standardlibrary.AimLock;
import net.minecraft.client.Mouse;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(Mouse.class)
public abstract class MouseMixin
{
    @Inject(method = "updateMouse", at = @At("HEAD"), cancellable = true)
    private void updateMouse(@NotNull CallbackInfo ci)
    {
        if (AimLock.isLocked()) ci.cancel();
    }
}
