package com.cubicequation.autokey_standardlibrary.entrypoints;

import com.cubicequation.autokey_core.api.AutokeyAPI;
import com.cubicequation.autokey_core.api.LoadAPI;
import com.cubicequation.autokey_core.entrypoints.Autokey;
import com.cubicequation.autokey_standardlibrary.api.KeyBindingsAPI;
import net.fabricmc.loader.api.metadata.ModMetadata;
import net.minecraft.client.option.KeyBinding;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

public class Keybindings implements LoadAPI
{
    private static final HashMap<String, KeyBinding> KEY_BINDINGS = new HashMap<>();

    @Override
    public @NotNull String onLoad(Autokey.@NotNull MessageConsumer consumer)
    {
        consumer.consume("Loading keybindings...", Autokey.MessageConsumer.Severity.INFO);

        KEY_BINDINGS.clear();
        AutokeyAPI.iterateAPIs("autokey_standardlibrary", KeyBindingsAPI.class, (KeyBindingsAPI api, ModMetadata metadata) ->
        {
            final Map<String, ? extends KeyBinding> keyBindings = api.getKeyBindings();
            KEY_BINDINGS.putAll(keyBindings);
            consumer.consume(String.format("Loaded %d keybindings from %s.", keyBindings.size(), metadata.getName()), Autokey.MessageConsumer.Severity.INFO);
        });

        return "keybindings";
    }

    public static KeyBinding getKeyBinding(final String name)
    {
        return KEY_BINDINGS.get(name);
    }
}
