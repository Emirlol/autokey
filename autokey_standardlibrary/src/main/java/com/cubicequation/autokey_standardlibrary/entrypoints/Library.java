package com.cubicequation.autokey_standardlibrary.entrypoints;

import com.cubicequation.autokey_core.api.LibraryAPI;
import com.cubicequation.autokey_core.language.functions.Function;
import com.cubicequation.autokey_core.language.functions.LibraryBuilder;
import com.cubicequation.autokey_standardlibrary.functions.*;

import java.util.Map;

public class Library implements LibraryAPI
{
    @Override
    public Map<String, Function> getFunctions()
    {
        final LibraryBuilder builder = new LibraryBuilder();

        Booleans.initialize(builder);
        Information.initialize(builder);
        Interactions.initialize(builder);
        Numbers.initialize(builder);
        Other.initialize(builder);
        Strings.initialize(builder);
        Arrays.initialize(builder);

        return builder.build();
    }
}
